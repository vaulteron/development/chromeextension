/*global chrome*/

import { GlobalState } from "./src/helper/globalState";
import { processRequestForContentScript } from "./src/helper/contentScriptReceiverManager";
import { findPasswordInputField, findUsernameInputField } from "./src/helper/inputElementsHelper";
import {
	isInputRendered,
	handleDomLoadedAndInputFieldsFound,
	handleOnBlurUsernameOrPasswordInput,
	handleOnFocusUsernameInput,
	showSavePasswords,
	handleDomUnloadedInputFields,
} from "./src/mainLogic";

import { sendRequestToWebWorker } from "../src/src-shared/webworkerClient";
import { logContent } from "../src/src-shared/logManager";

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
	processRequestForContentScript(request, sendResponse);

	return true; // This will make this communication async
});

window.addEventListener(
	"message",
	(event) => {
		// This event will be triggered by any message received. These messages could also be malicious to get data from Vaulteron

		// We only accept messages from ourselves
		if (event.source !== window) return;

		if (event.data.vaulteron_requestFromWebPage && event.data.vaulteron_requestFromWebPage === true) {
			logContent("got request from web page, relaying to webWorker: ", event.data);

			// Attach a flag show that this request is from any webpage and may be malicious. This allows the webworker to handle this request with caution
			const newPayload = { ...event.data.content.payload, isRequestFromWebPage: true };

			sendRequestToWebWorker(event.data.content.state, newPayload)
				.then((response) => window.postMessage({ vaulteron_responseFromExtension: true, content: response }, "*"))
				.catch((error) => window.postMessage({ vaulteron_responseFromExtension: true, content: error }, "*"));
		}
	},
	false
);

showSavePasswords();

const inputFields = document.getElementsByTagName("input");
const usernameInputField = findUsernameInputField(inputFields);
const passwordInputField = findPasswordInputField(inputFields);

GlobalState.usernameInputField = usernameInputField;
GlobalState.passwordInputField = passwordInputField;

if (usernameInputField && passwordInputField) {
	// Modify input field
	usernameInputField.autocomplete = "off";
	passwordInputField.autocomplete = "new-password";

	handleDomLoadedAndInputFieldsFound();

	usernameInputField.addEventListener("focus", handleOnFocusUsernameInput);

	usernameInputField.addEventListener("blur", handleOnBlurUsernameOrPasswordInput);
	passwordInputField.addEventListener("blur", handleOnBlurUsernameOrPasswordInput);

	usernameInputField.addEventListener("keydown", (event) => {
		if (event.key === "Enter") {
			handleOnBlurUsernameOrPasswordInput();
		}
	});
	passwordInputField.addEventListener("keydown", (event) => {
		if (event.key === "Enter") {
			handleOnBlurUsernameOrPasswordInput();
		}
	});

	const observer = new MutationObserver(() => {
		if (!isInputRendered()) {
			observer.disconnect();
			showSavePasswords();
			handleDomUnloadedInputFields();
		}
	});
	observer.observe(document, { attributes: false, characterData: false, childList: true, subtree: true });
}
