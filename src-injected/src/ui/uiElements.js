﻿// Importing the SVG does not work because the "viewbox" attribute will be removed. idk why.
// import svg from "bundle-text:../../../src/images/Logo.svg";

export const VaulteronLogoSvg = `
	<svg xmlns="http://www.w3.org/2000/svg" width="232.054" height="212.42" viewBox="0 0 232.054 212.42">
		<g id="svg_logo" transform="translate(-676.946 -1907)">
			<circle id="Ellipse_6" data-name="Ellipse 6" cx="17" cy="17" r="17" transform="translate(789 1979)" fill="#202349"/>
			<path id="Pfad_1818" data-name="Pfad 1818" d="M7.574,26.911h25.3l7.572,28.994H-1.475Z" transform="translate(786 1981)" fill="#202349"/>
			<g id="Ellipse_5" data-name="Ellipse 5" transform="translate(702 1907)" fill="none" stroke="#202349" stroke-width="17">
				<circle cx="103.5" cy="103.5" r="103.5" stroke="none"/>
				<circle cx="103.5" cy="103.5" r="95" fill="none"/>
			</g>
			<path id="Pfad_1821" data-name="Pfad 1821"
				  d="M-13.573-49.325S36.616-43.078,51.907-.858,62.462,60.1,85.08,82.719s54.479,35.309,54.479,35.309-26.006,12.92-88.8-34.172C13.458,52.646,3.442,24.158.713,8.988,6.392-31.238-13.573-49.325-13.573-49.325Z"
				  transform="translate(708 1987)" fill="#02598b"/>
			<path id="Pfad_1820" data-name="Pfad 1820"
				  d="M-22.081-32.426S30.152-22.4,45.876,14.867,61.207,64.3,78.4,83.15c20.855,22.861,61.18,34.836,61.18,34.836S91.865,122.272,29.075,75.18C-8.23,43.97,1.549,31-1.18,15.83A59.558,59.558,0,0,0-22.081-32.426Z"
				  transform="translate(708 1987)" fill="#008cc0"/>
			<path id="Pfad_1819" data-name="Pfad 1819"
				  d="M-31.054-10.763s52.375-11.2,80.162,57.941,90.528,70.809,90.528,70.809-50.92,37.923-113.71-9.17c-37.3-31.21-28.851-61.286-31.58-76.456C-13.411.042-31.054-10.763-31.054-10.763Z"
				  transform="translate(708 1987)" fill="#5ec4e6"/>
		</g>
	</svg>
`;

export const eCloseSVG = `
	<svg viewBox="0 0 24 24">
		<path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z" />
	</svg>
`;

export const circularProgress = `
	<div class="vaulteron-circular-progress-container">
		<span class="vaulteron-circular-progress" role="progressbar">
			<svg class="MuiCircularProgress-svg" viewBox="22 22 44 44">
				<circle class="MuiCircularProgress-circle MuiCircularProgress-circleIndeterminate css-14891ef" cx="44" cy="44" r="20.2" fill="none" stroke-width="3.6"></circle>
			</svg>
		</span>
	</div>
`;

export const generalNewPasswordFavicon = `
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512">
		<!-- Font Awesome Pro 5.15.4 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) -->
		<path d="M336.5 160C322 70.7 287.8 8 248 8s-74 62.7-88.5 152h177zM152 256c0 22.2 1.2 43.5 3.3 64h185.3c2.1-20.5 3.3-41.8 3.3-64s-1.2-43.5-3.3-64H155.3c-2.1 20.5-3.3 41.8-3.3 64zm324.7-96c-28.6-67.9-86.5-120.4-158-141.6 24.4 33.8 41.2 84.7 50 141.6h108zM177.2 18.4C105.8 39.6 47.8 92.1 19.3 160h108c8.7-56.9 25.5-107.8 49.9-141.6zM487.4 192H372.7c2.1 21 3.3 42.5 3.3 64s-1.2 43-3.3 64h114.6c5.5-20.5 8.6-41.8 8.6-64s-3.1-43.5-8.5-64zM120 256c0-21.5 1.2-43 3.3-64H8.6C3.2 212.5 0 233.8 0 256s3.2 43.5 8.6 64h114.6c-2-21-3.2-42.5-3.2-64zm39.5 96c14.5 89.3 48.7 152 88.5 152s74-62.7 88.5-152h-177zm159.3 141.6c71.4-21.2 129.4-73.7 158-141.6h-108c-8.8 56.9-25.6 107.8-50 141.6zM19.3 352c28.6 67.9 86.5 120.4 158 141.6-24.4-33.8-41.2-84.7-50-141.6h-108z"/>
	</svg>
`;

export const editIcon = `
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
		<!-- Font Awesome Pro 5.15.4 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) -->
		<path d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"/>
	</svg>	
`;
