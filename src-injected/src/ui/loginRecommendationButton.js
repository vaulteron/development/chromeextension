﻿import { eCloseSVG, VaulteronLogoSvg } from "./uiElements";
import { GlobalState } from "../helper/globalState";
import { sendRequestToWebWorker } from "../../../src/src-shared/webworkerClient";
import { CommunicationModelStates } from "../../../src/src-shared/helper/communicationModel";
import { apiBaseURL } from "../../../src/src-shared/settings";
import { appendElementAsIframeToBody } from "../helper/iframeHelper";

const loginRecommendationButton = "vaulteron-login-recommendation-button-id";
const iframeWrapperId_loginRecommendationButton = `vaulteron-iframe-wrapper__${loginRecommendationButton}`;

export const showLoginRecommendationButton = async () => {
	if (!GlobalState.usernameInputField || !GlobalState.passwordInputField) return;

	/**
	 *
	 * @type {ExtensionInfos}
	 */
	const infos = await sendRequestToWebWorker(CommunicationModelStates.getExtensionInfo, {});
	if (infos.isUserLoggedIn) return;

	await createOrGetLoginRecommendationButton(infos);
	const iframeWrapper = document.getElementById(iframeWrapperId_loginRecommendationButton);
	iframeWrapper.style.display = "block";
};

export const hideLoginRecommendationButton = () => {
	const iframeWrapper = document.getElementById(iframeWrapperId_loginRecommendationButton);
	if (iframeWrapper) iframeWrapper.style.display = "none";
};

/**
 *
 * @param {ExtensionInfos} infos
 * @returns Promise<{HTMLElement}>
 */
export const createOrGetLoginRecommendationButton = async (infos) => {
	const foundElement = document.getElementById(iframeWrapperId_loginRecommendationButton);
	if (foundElement) return foundElement.querySelector("iframe").contentDocument.querySelector(`#${loginRecommendationButton}`);

	const openModalButtonElement = document.createElement("div");
	openModalButtonElement.setAttribute("id", loginRecommendationButton);
	openModalButtonElement.innerHTML = `
		<div class="vaulteron-login-recommendation-button-container">
			<div class="vaulteron-logo-container">
				<div class="vaulteron-logo">${VaulteronLogoSvg}</div>
			</div>
			<div class="content-container">
				<label class="login-button">Bei Vaulteron anmelden</label>
				<div class="vaulteron-close-icon">${eCloseSVG}</div>
			</div>
		</div>
	`;

	const { iframe, iframeWrapper } = await appendElementAsIframeToBody(
		document.body,
		iframeWrapperId_loginRecommendationButton,
		openModalButtonElement,
		infos
	);
	iframeWrapper.style.position = "fixed !important";
	iframeWrapper.style.top = "0px !important";
	iframeWrapper.style.right = "0px !important";
	iframeWrapper.style.height = "0px !important";
	iframeWrapper.style.width = "0px !important";
	iframeWrapper.style.display = "none";

	openModalButtonElement.querySelector(".login-button").addEventListener("click", onOpenLoginTab);
	openModalButtonElement.querySelector(".vaulteron-logo-container").addEventListener("click", onOpenLoginTab);
	openModalButtonElement.querySelector(".vaulteron-close-icon").addEventListener("click", hideLoginRecommendationButton);

	return iframe.contentDocument.querySelector(`#${loginRecommendationButton}`);
};

const onOpenLoginTab = () => window.open(`${apiBaseURL}/login?ext=1`, "_blank").focus();
