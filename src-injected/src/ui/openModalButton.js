﻿import { VaulteronLogoSvg } from "./uiElements";
import { handleOpenModalButtonClicked, getPasswords } from "../mainLogic";
import { GlobalState } from "../helper/globalState";
import { createPopper } from "@popperjs/core/lib/popper-lite.js";
import preventOverflow from "@popperjs/core/lib/modifiers/preventOverflow.js";
import flip from "@popperjs/core/lib/modifiers/flip.js";

const openModalButtonId = "vaulteron-open-modal-button-id";

export const showOpenModalButton = async () => {
	if (!GlobalState.usernameInputField || !GlobalState.passwordInputField) return;

	const passwords = await getPasswords();
	if (passwords.length === 0) return;

	let element = createOrGetOpenModalButton();
	element.classList.add("visible");
};

export const hideOpenModalButton = () => {
	let element = createOrGetOpenModalButton();

	element.classList.remove("visible");
};

export const createOrGetOpenModalButton = () => {
	const foundElement = document.getElementById(openModalButtonId);
	if (foundElement) return foundElement;

	const eInputUsernameCoords = GlobalState.usernameInputField.getBoundingClientRect();

	const openModalButtonElement = document.createElement("div");
	openModalButtonElement.setAttribute("id", openModalButtonId);
	openModalButtonElement.style.visibility = "hidden";
	openModalButtonElement.innerHTML = `
		<div class="vaulteron-open-modal-button-container">
			<div class="vaulteron-logo-container" style="height: ${eInputUsernameCoords.height - 4}px; width: ${eInputUsernameCoords.height - 4}px;">
				<div class="vaulteron-logo">${VaulteronLogoSvg}</div>
			</div>
		</div>
	`;

	document.body.appendChild(openModalButtonElement);
	openModalButtonElement.addEventListener("click", handleOpenModalButtonClicked);

	createPopper(GlobalState.usernameInputField, openModalButtonElement, {
		placement: "right",
		modifiers: [preventOverflow, flip],
	});

	return openModalButtonElement;
};
