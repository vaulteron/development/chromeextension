﻿import { editIcon, VaulteronLogoSvg } from "./uiElements";
import { getPasswords, handleOnSaveEnteredPassword, isInputRendered, sendEmptyValuesToWebWorker } from "../mainLogic";
import { GlobalState } from "../helper/globalState";
import { CommunicationModelStates } from "../../../src/src-shared/helper/communicationModel";
import { errorContent } from "../../../src/src-shared/logManager";
import { sendRequestToWebWorker } from "../../../src/src-shared/webworkerClient";

import svgLock from "bundle-text:@fortawesome/fontawesome-free/svgs/solid/lock.svg";
import svgUser from "bundle-text:@fortawesome/fontawesome-free/svgs/solid/user.svg";
import svgUserSecret from "bundle-text:@fortawesome/fontawesome-free/svgs/solid/user-secret.svg";
import { appendElementAsIframeToBody } from "../helper/iframeHelper";

const saveEnteredPasswordPopupId = "vaulteron-open-save-entered-password-popup-id";
const iframeWrapperId_saveEnteredPasswordPopup = `vaulteron-iframe-wrapper__${saveEnteredPasswordPopupId}`;

export const showSaveEnteredPasswordPopup = async () => {
	try {
		/**
		 *
		 * @type {ExtensionInfos}
		 */
		const infos = await sendRequestToWebWorker(CommunicationModelStates.getExtensionInfo, {});
		if (!infos.isUserLoggedIn) return;

		// // TODO: remove debugging code
		// GlobalState.passwordToSave.password = "jdsalkf";
		// GlobalState.passwordToSave.username = "my-cool-username@email.com";
		// GlobalState.passwordToSave.url = "http://google.com";
		// let e2 = await createOrGetSaveEnteredPasswordPopup(infos);
		// const i2 = document.getElementById(iframeWrapperId_saveEnteredPasswordPopup);
		// i2.style.display = "block";
		// setData(e2);
		// return;

		const pw = await sendRequestToWebWorker(CommunicationModelStates.getEnteredPassword, {});
		savePWToGlobalState(pw);
		if (!hasData()) return;

		if (isInputRendered()) return;

		const storedPasswords = await getPasswords(pw.url);
		if (storedPasswords.length > 0) {
			const foundPW = storedPasswords.find((pw) => pw.login === GlobalState.passwordToSave.username);
			if (foundPW) {
				const decryptedPasswordString = await sendRequestToWebWorker(CommunicationModelStates.requestPasswordString, {
					id: foundPW.id,
					isAccountPassword: foundPW.type === "Private",
				});
				if (decryptedPasswordString !== GlobalState.passwordToSave.password) {
					GlobalState.passwordToSave.existingPasswordId = foundPW.id;
					GlobalState.passwordToSave.existingPasswordModifyLock = foundPW.modifyLock;
				} else {
					return; // Exactly the same password is already stored in Vaulteron
				}
			}
		}

		let element = await createOrGetSaveEnteredPasswordPopup(infos);
		const iframeWrapper = document.getElementById(iframeWrapperId_saveEnteredPasswordPopup);
		iframeWrapper.style.display = "block";
		setData(element);
	} catch (e) {
		errorContent(e);
	}
};

export const hideSaveEnteredPasswordPopup = async () => {
	const iframeWrapper = document.getElementById(iframeWrapperId_saveEnteredPasswordPopup);
	if (iframeWrapper) iframeWrapper.style.display = "none";

	await sendEmptyValuesToWebWorker();
};

/**
 *
 * @param {ExtensionInfos} infos
 * @returns Promise<{HTMLElement}>
 */
export const createOrGetSaveEnteredPasswordPopup = async (infos) => {
	const foundElement = document.getElementById(iframeWrapperId_saveEnteredPasswordPopup);
	if (foundElement) return foundElement.querySelector("iframe").contentDocument.querySelector(`#${saveEnteredPasswordPopupId}`);

	const isUpdate = Boolean(GlobalState.passwordToSave.existingPasswordId);
	const isExistingSharedPassword = Boolean(GlobalState.passwordToSave.existingPasswordModifyLock);

	const savePasswordPopupElement = document.createElement("div");
	savePasswordPopupElement.setAttribute("id", saveEnteredPasswordPopupId);
	savePasswordPopupElement.innerHTML = `
		<div class="vaulteron-open-save-entered-password-popup-container">
			<div class="vaulteron-modal-header">
				<div class="vaulteron-logo">${VaulteronLogoSvg}</div>
				<h2 class="vaulteron-heading">Vaulteron</h2>
			</div>
			<div class="vaulteron-modal-content">
				<div class="text">${isUpdate ? "Passwort aktualisieren?" : "Neues Passwort speichern?"}</div>
				<div class="data-container mui-container-fluid">
					<div class="mui-row">
						<div class="column column-favicon mui-col-xs-2">
							<img class="favicon" alt="favicon" src=""/>
						</div>
						<div class="column column-data mui-col-xs-8">
							<label class="username-text"></label>
							<label class="url-text"></label>
						</div>
						<div class="column column-edit mui-col-xs-2">
							<div class="edit-icon">${editIcon}</div>
						</div>
					</div>
					${
						infos.myself.mandator.isBusinessCustomer && !isUpdate
							? `
							<div class="mui-row">
								<div class="column mui-col-xs-2">
									<div class="icon-left password-type">${svgUserSecret}</div>
								</div>
								<div class="mui-col-xs-10">
									<div class="type-select-container mui-select">
										<select>
											<option value="Private" selected>Privataccount</option>
											<option value="Company">Firmenaccount</option>
										</select>
										<label>Speicherort</label>
									</div>
								</div>
							</div>
							`
							: ``
					}
					<div class="edit-container">
						<div class="mui-row">
							<div class="column mui-col-xs-2">
								<div class="icon-left">${svgUser}</div>
							</div>
							<div class="mui-col-xs-10">
								<div class="mui-textfield mui-textfield--float-label">
									<input type="text" class="username">
									<label>Benutzername</label>
								</div>
							</div>
						</div>
						<div class="mui-row">
							<div class="column mui-col-xs-2">
								<div class="icon-left">${svgLock}</div>
							</div>
							<div class="mui-col-xs-10">
								<div class="mui-textfield mui-textfield--float-label">
									<input type="text" class="password">
									<label>Neues Passwort</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="buttons">
					<button class="cancel-button mui-btn mui-btn--flat">Abbrechen</button>
					<button class="save-button mui-btn mui-btn--primary mui-btn--raised">${isUpdate ? "Aktualisieren" : "Speichern"}</button>
				</div>
			</div>
		</div>
	`;

	const { iframe, iframeWrapper } = await appendElementAsIframeToBody(
		document.body,
		iframeWrapperId_saveEnteredPasswordPopup,
		savePasswordPopupElement,
		infos
	);
	iframeWrapper.style.position = "absolute !important";
	iframeWrapper.style.top = "0px !important";
	iframeWrapper.style.left = "0px !important";
	iframeWrapper.style.height = "0px !important";
	iframeWrapper.style.width = "0px !important";
	iframeWrapper.style.display = "none";
	iframe.style.position = "fixed";
	iframe.style.top = "0px";
	iframe.style.left = "calc(50vw - 250px)";
	iframe.style.width = "500px";
	iframe.style.height = "500px";
	iframe.style.zIndex = "2000000000";
	iframe.style.border = "none";

	savePasswordPopupElement.querySelector(".cancel-button").addEventListener("click", hideSaveEnteredPasswordPopup);
	savePasswordPopupElement.querySelector(".save-button").onclick = () => handleOnSaveEnteredPassword();

	savePasswordPopupElement
		.querySelector(".edit-icon")
		.addEventListener("click", () => savePasswordPopupElement.querySelector(".edit-container").classList.toggle("visible"));

	if (!infos.myself.mandator.isBusinessCustomer) {
		GlobalState.passwordToSave.type = "Private";
	} else {
		if (isUpdate) {
			if (isExistingSharedPassword) GlobalState.passwordToSave.type = "Company";
			else GlobalState.passwordToSave.type = "Private";
		} else {
			savePasswordPopupElement.querySelector(".type-select-container select").addEventListener("change", () => {
				const newValue = savePasswordPopupElement.querySelector(".type-select-container select").value;
				GlobalState.passwordToSave.type = newValue;
				const eIcon = savePasswordPopupElement.querySelector(".icon-left.password-type");
				if (!eIcon) return;
				switch (newValue) {
					case "Private":
						eIcon.innerHTML = svgUserSecret;
						break;
					case "Company":
						eIcon.innerHTML = svgLock;
						break;
				}
			});
		}
	}

	return iframe.contentDocument.querySelector(`#${saveEnteredPasswordPopupId}`);
};

/* ############################################## local logic ############################################## */
/* ############################################## local logic ############################################## */
/* ############################################## local logic ############################################## */

const setData = (element) => {
	const { hostname } = new URL(GlobalState.passwordToSave.url);

	element.querySelector(".username-text").innerText = GlobalState.passwordToSave.username;
	element.querySelector(".url-text").innerText = hostname;
	element.querySelector(".favicon").setAttribute("src", `https://www.google.com/s2/favicons?domain=${hostname}&sz=128`);
	element.querySelector(".username").value = GlobalState.passwordToSave.username;
	element.querySelector(".password").value = GlobalState.passwordToSave.password;
};

function hasData() {
	return GlobalState.passwordToSave.url && GlobalState.passwordToSave.username && GlobalState.passwordToSave.password;
}

function savePWToGlobalState(pw) {
	GlobalState.passwordToSave.username = pw.username;
	GlobalState.passwordToSave.password = pw.password;
	GlobalState.passwordToSave.url = pw.url;
}
