﻿import { showOpenModalButton } from "./openModalButton";
import { circularProgress, eCloseSVG, VaulteronLogoSvg } from "./uiElements";
import { handleOnPasswordChosen } from "../mainLogic";
import { GlobalState } from "../helper/globalState";
import { sendRequestToWebWorker } from "../../../src/src-shared/webworkerClient";
import { CommunicationModelStates } from "../../../src/src-shared/helper/communicationModel";
import { appendElementAsIframeToBody } from "../helper/iframeHelper";

const modalElementId = "vaulteron-modal-id";
const iframeWrapperId_modal = `vaulteron-iframe-wrapper__${modalElementId}`;

export const showModal = async () => {
	/**
	 *
	 * @type {ExtensionInfos}
	 */
	const infos = await sendRequestToWebWorker(CommunicationModelStates.getExtensionInfo, {});

	await createOrGetModal(infos);
	const modalWrapper = document.getElementById(modalElementId);
	modalWrapper.classList.add("visible");
	document.body.classList.add("vaulteron-modal-open");

	GlobalState.wasModalOpenedAtLeastOnce = true;
};
export const hideModal = async () => {
	const modalWrapper = document.getElementById(modalElementId);
	modalWrapper.classList.remove("visible");
	document.body.classList.remove("vaulteron-modal-open");

	await showOpenModalButton();
};

/**
 *
 * @param {ExtensionInfos | null} infos
 * @returns Promise<{HTMLElement | null}>
 */
export const createOrGetModal = async (infos) => {
	const foundElement = document.getElementById(iframeWrapperId_modal);
	if (foundElement) return foundElement.querySelector("iframe").contentDocument.querySelector(`#${modalElementId}_content`);

	if (infos == null) return null;

	const modalContentElement = document.createElement("div");
	modalContentElement.setAttribute("id", `${modalElementId}_content`);
	modalContentElement.innerHTML = `
		<div class="vaulteron-modal-header">
			<div class="vaulteron-logo">${VaulteronLogoSvg}</div>
			<h2 class="vaulteron-heading">Vaulteron</h2>
			<div class="vaulteron-close-icon">${eCloseSVG}</div>
		</div>
		<div class="vaulteron-password-list">
			<div id="vaulteron-password-container">${circularProgress}</div>
		</div>
		<br>
		<div id="vaulteron-error-container"></div>
	`;

	const eModalWithBackdropContainer = document.createElement("div");
	eModalWithBackdropContainer.setAttribute("id", modalElementId);
	eModalWithBackdropContainer.style.visibility = "hidden";
	eModalWithBackdropContainer.innerHTML = `
		<div class="vaulteron-backdrop"></div>
		<div class="vaulteron-modal-container"/>
	`;
	document.body.appendChild(eModalWithBackdropContainer);
	const iframeWrapperParent = eModalWithBackdropContainer.querySelector(".vaulteron-modal-container");
	const { iframe, iframeWrapper } = await appendElementAsIframeToBody(iframeWrapperParent, iframeWrapperId_modal, modalContentElement, infos);
	iframeWrapper.style.position = "absolute !important";
	iframeWrapper.style.top = "0px !important";
	iframeWrapper.style.left = "0px !important";
	iframeWrapper.style.height = "0px !important";
	iframeWrapper.style.width = "0px !important";
	iframeWrapper.style["text-align"] = "left";
	iframe.style.position = "fixed";
	iframe.style.width = "300px";
	iframe.style.height = "100vh";
	iframe.style.zIndex = "2000000000";
	iframe.style.border = "none";

	eModalWithBackdropContainer.querySelector(`.vaulteron-backdrop`).addEventListener("click", hideModal);
	modalContentElement.querySelector(`.vaulteron-close-icon`).addEventListener("click", hideModal);

	if (!infos.isUserLoggedIn) {
		showModalError("Sie sind leider nicht angemeldet.");
	}

	return iframe.contentDocument.querySelector(`#${modalElementId}_content`);
};

export const showModalError = (errorString) => {
	const e = document.getElementById("vaulteron-error-container");
	if (e) e.innerHTML = `Es ist ein Fehler aufgetreten: <br/>${errorString}`;
};

/**
 *
 * @param {SharedPasswordType[]} passwords
 */
export const generatePasswordListHtmlAndUpdateModal = async (passwords) => {
	/**
	 *
	 * @type {ExtensionInfos}
	 */
	const infos = await sendRequestToWebWorker(CommunicationModelStates.getExtensionInfo, {});
	const modalElement = await createOrGetModal(infos);
	const ePasswordContainer = modalElement.querySelector(`#vaulteron-password-container`);

	const setInnerHtmlOfPasswordList = (html) => (ePasswordContainer.innerHTML = html);

	if (passwords.length === 0) {
		const html = `<h5>Keine Passwörter gefunden</h5>`;
		setInnerHtmlOfPasswordList(html);
		return;
	}

	// Remove current child -> the loading spinner
	setInnerHtmlOfPasswordList("");

	const ePasswordList = document.createElement("div");
	ePasswordList.classList.add("vaulteron-password-list-root");

	for (let password of passwords) {
		const ePasswordItem = document.createElement("div");
		ePasswordItem.id = `vaulteron-password-id-${password.id}`;
		ePasswordItem.classList.add("vaulteron-password-list-item");
		ePasswordItem.innerHTML = `
			<div class="vaulteron-password-list-item-name">${password.name}</div>
			<div class="vaulteron-password-list-item-login">${password.login}</div>
		`;
		ePasswordList.appendChild(ePasswordItem);
		const element = ePasswordList.querySelector(`#vaulteron-password-id-${password.id}`);
		element.addEventListener("click", () => handleOnPasswordChosen(password));
	}

	ePasswordContainer.appendChild(ePasswordList);
};
