﻿import { generatePasswordListHtmlAndUpdateModal, hideModal, showModal, showModalError } from "./ui/modal";
import { CommunicationModelStates } from "../../src/src-shared/helper/communicationModel";
import { hideOpenModalButton, showOpenModalButton } from "./ui/openModalButton";
import { fillPassword } from "./helper/inputElementsHelper";
import { GlobalState } from "./helper/globalState";
import { hideSaveEnteredPasswordPopup, showSaveEnteredPasswordPopup } from "./ui/saveEnteredPasswordPopup";
import { errorContent } from "../../src/src-shared/logManager";
import { sendRequestToWebWorker } from "../../src/src-shared/webworkerClient";
import { showLoginRecommendationButton } from "./ui/loginRecommendationButton";

/**
 *
 * @type {SharedPasswordType[]}
 */
let fetchedPasswords = null;

/* ############################################## public events ############################################## */
/* ############################################## public events ############################################## */
/* ############################################## public events ############################################## */

export const handleDomLoadedAndInputFieldsFound = async () => {
	await showOpenModalButton();
	await showLoginRecommendationButton();
};

export const handleDomUnloadedInputFields = async () => {
	await hideOpenModalButton();
}

export const handleOnFocusUsernameInput = () => {
	// if (!GlobalState.wasModalOpenedAtLeastOnce) openModalAndReloadData();
};
export const handleOpenModalButtonClicked = () => openModalAndReloadData();

/**
 *
 * @param {SharedPasswordType} password
 */
export const handleOnPasswordChosen = (password) => fetchPasswordStringAndFillInputs(password);

export const handleOnBlurUsernameOrPasswordInput = () => {
	sendCurrentInsertedValuesToWebWorker();
	showSavePasswords();
};

export const showSavePasswords = async () => await showSaveEnteredPasswordPopup();

export const handleOnSaveEnteredPassword = () => {
	sendRequestToWebWorker(CommunicationModelStates.saveEnteredPassword, {
		username: GlobalState.passwordToSave.username,
		password: GlobalState.passwordToSave.password,
		url: GlobalState.passwordToSave.url,
		type: GlobalState.passwordToSave.type,
		existingPasswordId: GlobalState.passwordToSave.existingPasswordId,
		existingPasswordModifyLock: GlobalState.passwordToSave.existingPasswordModifyLock,
	}).then(hideSaveEnteredPasswordPopup);
};

/* ############################################## actual logic ############################################## */
/* ############################################## actual logic ############################################## */
/* ############################################## actual logic ############################################## */

const openModalAndReloadData = () => {
	if (fetchedPasswords?.length > 0) {
		showModal().then(() => {
			generatePasswordListHtmlAndUpdateModal(fetchedPasswords);
		});
		return;
	}
	sendRequestToWebWorker(CommunicationModelStates.requestPasswords, window.location.origin)
		.then((passwords) => {
			fetchedPasswords = passwords;
			if (fetchedPasswords.length > 0) {
				showModal().then(() => {
					generatePasswordListHtmlAndUpdateModal(fetchedPasswords);
				});
			}
		})
		.catch((errorJsonObject) => {
			showModalError(errorJsonObject.message);
		});
};

/**
 *
 * @param {string | null} url
 * @returns {Promise<SharedPasswordType[]>}
 */
export const getPasswords = async (url = null) => {
	if (url === null) url = window.location.origin;

	if (!fetchedPasswords || fetchedPasswords?.length > 0) {
		try {
			fetchedPasswords = await sendRequestToWebWorker(CommunicationModelStates.requestPasswords, url);
		} catch (e) {
			errorContent("content> error when fetching passwords through webworker: ", e);
			return [];
		}
	}
	return fetchedPasswords;
};

/**
 *
 * @param {SharedPasswordType} password
 */
const fetchPasswordStringAndFillInputs = (password) => {
	sendRequestToWebWorker(CommunicationModelStates.requestPasswordString, {
		id: password.id,
		isAccountPassword: password.type === "Private",
	})
		.then((passwordString) => {
			fillPassword(password.login, passwordString);
			hideModal();
		})
		.catch((errorJsonObject) => {
			showModalError(errorJsonObject.message);
		});
};

const sendCurrentInsertedValuesToWebWorker = () => {

	const username = GlobalState.usernameInputField.value;
	const password = GlobalState.passwordInputField.value;

	if (username && password) {
		sendRequestToWebWorker(CommunicationModelStates.saveUsernameAndPassword, { username, password });
	}
};

export const sendEmptyValuesToWebWorker = async () => await sendRequestToWebWorker(CommunicationModelStates.clearStoredUsernameAndPassword, {});

export const isInputRendered = () => document.contains(GlobalState.usernameInputField) && document.contains(GlobalState.passwordInputField);
