﻿export const GlobalState = {
	wasModalOpenedAtLeastOnce: false,
	usernameInputField: null,
	passwordInputField: null,
	passwordToSave: {
		username: null,
		password: null,
		url: null,
		type: "Private",
		existingPasswordId: null,
		existingPasswordModifyLock: null,
	},
};
