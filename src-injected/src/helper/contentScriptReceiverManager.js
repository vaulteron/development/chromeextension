﻿import { CommunicationModelStates } from "../../../src/src-shared/helper/communicationModel";
import { fillPassword } from "./inputElementsHelper";

/**
 *
 * @param {CommunicationModel} request
 * @param {function} sendResponse
 */
export const processRequestForContentScript = (request, sendResponse) => {
	switch (request.state) {
		case CommunicationModelStates.fillPassword:
			fillPassword(request.payload.login, request.payload.passwordString);
			sendResponse(true);
			break;
		default:
			sendResponse("unknown request type");
	}
};
