﻿import { GlobalState } from "./globalState";

export const fillPassword = (username, password) => {
	GlobalState.usernameInputField.setAttribute("value", username);
	GlobalState.passwordInputField.setAttribute("value", password);
};

/**
 *
 * @param {HTMLCollectionOf<HTMLInputElement>} inputFields
 * @returns {HTMLInputElement | null}
 */
export const findPasswordInputField = (inputFields) => {
	for (const inputField of inputFields) {
		if (isPasswordInput(inputField)) {
			return inputField;
		}
	}
};

/**
 *
 * @param {HTMLInputElement} inputField
 * @returns {boolean}
 */
export const isPasswordInput = (inputField) => inputField.type === "password";

// Todo check for opacity too (manu: and visibility?)
/**
 *
 * @param {HTMLInputElement} inputField
 * @returns {boolean}
 */
export const isHiddenInput = (inputField) => {
	const computedStyle = window.getComputedStyle(inputField);

	return inputField.type === "hidden" || computedStyle.visibility === "hidden" || computedStyle.display === "none";
};

/**
 *
 * @param {string} keyWord
 * @param {string} value
 * @returns {boolean}
 */
export const includesKeyword = (keyWord, value) => {
	if (!keyWord.trim() || !value.trim()) return false;

	const lowerCaseKeyWord = keyWord.toLowerCase();
	const lowerCaseValue = value.toLowerCase();

	return lowerCaseKeyWord.includes(lowerCaseValue) || lowerCaseValue.includes(lowerCaseKeyWord);
};

/**
 *
 * @param {HTMLCollectionOf<HTMLInputElement>} inputFields
 * @returns {HTMLInputElement | null}
 */
export const findUsernameInputField = (inputFields) => {
	if (inputFields.length === 0) return null;

	let bestInputFieldForUsername = inputFields[0];
	let maxScore = 0;

	for (const inputField of inputFields) {
		let attributes = inputField.attributes;
		let score = 0;

		for (const attribute of attributes) {
			let nodeName = attribute.nodeName;
			if (nodeName && inputField && !isHiddenInput(inputField)) {
				const value = inputField.getAttribute(nodeName);
				for (let keyWord of userNameKeyWords) {
					if (includesKeyword(keyWord, value)) {
						score++;
					}
				}
			}

			if (score > maxScore) {
				bestInputFieldForUsername = inputField;
				maxScore = score;
			}
		}
	}

	if (maxScore === 0) return null;

	return bestInputFieldForUsername;
};

const userNameKeyWords = [
	"username",
	"user name",
	"email",
	"email address",
	"e-mail",
	"e-mail address",
	"userid",
	"user id",
	"customer id",
	"login id",
	"benutzername",
	"benutzer name",
	"email adresse",
	"e-mail adresse",
	"benutzerid",
	"benutzer id",
];
