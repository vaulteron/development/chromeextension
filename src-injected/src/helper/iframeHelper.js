﻿/**
 *
 * @param {HTMLElement} parentToAttachTo
 * @param {string} iframeWrapperId
 * @param {HTMLElement} bodyContent
 * @param {ExtensionInfos} infos
 * @returns Promise<{iframeWrapper: HTMLDivElement, iframe: HTMLIFrameElement}>
 */
export const appendElementAsIframeToBody = async (parentToAttachTo, iframeWrapperId, bodyContent, infos) => {
	return new Promise((resolve, reject) => {
		const iframeWrapper = document.createElement("div");
		iframeWrapper.setAttribute("id", iframeWrapperId);

		const iframe = document.createElement("iframe");
		iframe.onload = () => {
			const cssLink = document.createElement("link");
			cssLink.rel = "stylesheet";
			cssLink.href = `${infos.baseFileUrl}contentScript.css`;

			const muiJs = document.createElement("script");
			muiJs.src = `${infos.baseFileUrl}libs/mui.min.js`;
			muiJs.type = "text/javascript";

			iframe.contentWindow.document.querySelector("head").appendChild(cssLink);
			iframe.contentWindow.document.querySelector("head").appendChild(muiJs);
			iframe.contentWindow.document.querySelector("body").appendChild(bodyContent);
			iframe.contentWindow.document.querySelector("body").style.transition = "unset";
			iframe.contentWindow.document.querySelector("body").style.overflow = "hidden";
			iframe.contentWindow.document.querySelector("html").style.overflow = "hidden";
			resolve({ iframe, iframeWrapper });
		};
		iframeWrapper.appendChild(iframe);
		parentToAttachTo.appendChild(iframeWrapper);
	});
};
