﻿/*global chrome*/

import { fetchMyself, fetchPasswordsForUrl, fetchPasswordString, savePassword } from "../src/src-shared/apiClient";
import { CommunicationModelStates, createEmptySuccessObject, createErrorObject, createSuccessObject } from "../src/src-shared/helper/communicationModel";
import { closeVaulteronExtensionLoginPage, getExtensionFileBaseUrl, getExtensionId, getUrlForTab } from "../src/src-shared/managers/chromeManager";
import { StorageManager } from "../src/src-shared/managers/storageManager";
import { errorWebWorker, logWebWorker } from "../src/src-shared/logManager";
import { api } from "../src/src-shared/managers/apiManager";

/* ############################################## messaging ############################################## */
/* ############################################## messaging ############################################## */
/* ############################################## messaging ############################################## */

chrome.runtime.onMessage.addListener((req, sender, sendResponse) => handleMessageReceived(req, sender, sendResponse));

function handleMessageReceived(req, sender, sendResponse) {
	/**
	 * @type {CommunicationModel}
	 */
	const request = req;

	if (request.payload?.isRequestFromWebPage) logWebWorker("received request: (request was from webpage)", request);
	else logWebWorker("received request: ", request);

	const f = async () => {
		try {
			if (sender.tab && request.payload?.isRequestFromWebPage) {
				const requestUrl = await getUrlForTab(sender.tab.id);
				const requestUrlHostname = new URL(requestUrl).host;
				const allowedHosts = ["app.vaulteron.", "staging.vaulteron.", ".dev.vaulteron.com", "https://localhost:3000/"];
				if (!allowedHosts.some((allowedHost) => requestUrlHostname.includes(allowedHost))) {
					const allowedRequests = [
						CommunicationModelStates.requestPasswords,
						CommunicationModelStates.requestPasswordString,
						CommunicationModelStates.saveUsernameAndPassword,
						CommunicationModelStates.saveEnteredPassword,
						CommunicationModelStates.getEnteredPassword,
						CommunicationModelStates.clearStoredUsernameAndPassword,
						CommunicationModelStates.clearWebWorkerStorage,
						CommunicationModelStates.getExtensionInfo,
					];
					if (!allowedRequests.includes(request.state)) return createErrorObject(-1, "unauthorized extension access");
				}
			}

			switch (request.state) {
				case CommunicationModelStates.requestPasswords:
					const targetUrl = request.payload?.isRequestFromWebPage ? requestUrl : request.payload; // Don´t allow to request any url when request is from untrusted source
					return createSuccessObject(CommunicationModelStates.response_fetchedPasswords, await fetchPasswordsForUrl(targetUrl));
				case CommunicationModelStates.requestPasswordString:
					return createSuccessObject(
						CommunicationModelStates.response_fetchedPasswordString,
						await fetchPasswordString(request.payload.id, request.payload.isAccountPassword)
					);
				case CommunicationModelStates.saveUsernameAndPassword:
					await handleStoreUsernameAndPassword(sender.tab.id, request.payload.username, request.payload.password);
					return createEmptySuccessObject();
				case CommunicationModelStates.saveEnteredPassword:
					await handleSaveEnteredPassword(
						sender.tab.id,
						request.payload.username,
						request.payload.password,
						request.payload.url,
						request.payload.type,
						request.payload.existingPasswordId,
						request.payload.existingPasswordModifyLock
					);
					return createEmptySuccessObject();
				case CommunicationModelStates.getEnteredPassword:
					return createSuccessObject(CommunicationModelStates.response_storedPassword, await getPasswordFromStorage());
				case CommunicationModelStates.clearStoredUsernameAndPassword:
					await handleStoreUsernameAndPassword(sender.tab.id, "", "");
					return createEmptySuccessObject();

				// General stuff
				case CommunicationModelStates.clearWebWorkerStorage:
					await clearStorage();
					return createEmptySuccessObject();

				// Vaulteron app stuff
				case CommunicationModelStates.vaulteronLogin:
					const username = request.payload.username;
					const passwordHash = request.payload.passwordHash;
					const webAuthnAssertionResponse = request.payload.webAuthnAssertionResponse;
					const totpCode = request.payload.totpCode;
					const closeTabOnCompletion = request.payload.closeTabOnCompletion;

					if (Boolean(totpCode)) {
						await loginWithTOPT(totpCode, passwordHash);
					} else if (Boolean(webAuthnAssertionResponse)) {
						await loginWithWebAuthn(webAuthnAssertionResponse, passwordHash);
					} else {
						await login(username, passwordHash);
					}

					if (closeTabOnCompletion) await closeVaulteronExtensionLoginPage();
					return createEmptySuccessObject();
				case CommunicationModelStates.getExtensionInfo:
					let myself = null;
					try {
						myself = await StorageManager.getMyself();
					} catch (e) {
						// Ignored
					}
					/**
					 *
					 * @type {ExtensionInfos}
					 */
					const infos = {
						isUserLoggedIn: Boolean(myself),
						myself: myself,
						extensionId: getExtensionId(),
						baseFileUrl: getExtensionFileBaseUrl(),
					};
					return createSuccessObject(CommunicationModelStates.getExtensionInfo, infos);

				default:
					return createErrorObject(-1, `Unknown command was sent to WebWorker: ${JSON.stringify(request)}`);
			}
		} catch (e) {
			if (e.state && e.error) throw e;

			errorWebWorker("webworker> error occured on request: ", request, e);
			throw createErrorObject(e.httpCode, e.message);
		}
	};
	/**
	 *
	 * @param {CommunicationModel} communicationModel
	 */
	const sendResponseAndLog = (communicationModel) => {
		if (request.payload?.isRequestFromWebPage) logWebWorker(`sending response with data: (request was from webpage})`, communicationModel);
		else logWebWorker(`sending response with data: `, communicationModel);

		if (communicationModel.error && communicationModel.error.httpCode === 401)
			clearStorage()
				.then(() => sendResponse(communicationModel))
				.catch(() => sendResponse(communicationModel));
		else sendResponse(communicationModel);
	};
	f().then(sendResponseAndLog).catch(sendResponseAndLog);

	return true; // This will make this communication async
}

/* ############################################## local logic ############################################## */
/* ############################################## local logic ############################################## */
/* ############################################## local logic ############################################## */

const fetchMyselfAndSaveToChromeStorage = async (hashedPassword) => {
	const myselfResult = await fetchMyself();
	await StorageManager.saveLoginPasswordHash(hashedPassword);
	await StorageManager.saveEncryptedPrivateKey(myselfResult.data.me.keyPair.encryptedPrivateKey);
	await StorageManager.saveMyself(myselfResult.data.me);
};

const login = async (email, hashedPassword) => {
	let response = await fetch(api.login, {
		method: "POST",
		headers: new Headers({ "Content-Type": "application/json" }),
		body: JSON.stringify({
			email: email,
			password: hashedPassword,
		}),
	});

	if (response.ok) {
		await response.text(); // Must be called so that the request can be properly finished(/read)
		await fetchMyselfAndSaveToChromeStorage(hashedPassword);
	} else {
		const err = await response.json();
		if (err.errorCode === 33) {
			// ErrorCode 33: 2FA is required to log in. Values must be set.
		}
		throw Error(JSON.stringify(err));
	}
};

const loginWithTOPT = async (totp, hashedPassword) => {
	let response = await fetch(api.twoFactorTotpLogin, {
		method: "POST",
		headers: new Headers({ "Content-Type": "application/json" }),
		body: JSON.stringify({
			code: totp,
			isPersistent: true,
			rememberClient: false,
		}),
	});

	if (!response.ok) {
		const err = await response.json();
		throw Error(JSON.stringify(err));
	}

	await fetchMyselfAndSaveToChromeStorage(hashedPassword);
	return true;
};

const loginWithWebAuthn = async (assertionResponse, hashedPassword) => {
	let response = await fetch(api.twoFactorWebAuthnLogin, {
		method: "POST",
		headers: new Headers({ "Content-Type": "application/json" }),
		body: JSON.stringify({
			assertionResponse: assertionResponse,
			isPersistent: true,
		}),
	});

	if (!response.ok) {
		const err = await response.json();
		throw Error(JSON.stringify(err));
	}

	await fetchMyselfAndSaveToChromeStorage(hashedPassword);
	return true;
};

const handleStoreUsernameAndPassword = async (tabId, username, password) => {
	const url = await getUrlForTab(tabId);
	await savePasswordToStorage(tabId, username, password, url);
};

const handleSaveEnteredPassword = async (tabId, username, password, url, type, existingPasswordId, existingPasswordModifyLock) => {
	const myself = await StorageManager.getMyself();
	try {
		logWebWorker(tabId, `### Saving password: username: ${username} password: ${password} url: ${url} type: ${type}`);
		await savePassword(myself, myself.keyPair.publicKeyString, username, password, url, type, existingPasswordId, existingPasswordModifyLock);
	} catch (e) {
		logWebWorker(tabId, `error saving password: ${e.message}`);
	}
};

async function savePasswordToStorage(tabId, username = null, password = null, url = null) {
	return await chrome.storage.local.set({ pwToSave: JSON.stringify({ tabId, username, password, url }) });
}

async function getPasswordFromStorage() {
	const data = await chrome.storage.local.get({ pwToSave: "{}" });
	return await JSON.parse(data.pwToSave);
}

async function clearStorage() {
	const names = await caches.keys();
	for (let name of names) await caches.delete(name);

	let response = await fetch(api.logout, {
		method: "POST",
		headers: new Headers({ "Content-Type": "application/json" }),
		body: JSON.stringify({}),
		credentials: "include",
	});
	if (!response.ok && response.status !== 401) throw new Error(`Unable to logout: ${await response.text()}`);

	await StorageManager.clearExtensionStorage();
}
