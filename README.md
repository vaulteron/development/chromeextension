# Chrome/Firefox Extension

# How to develop

Adjust the settings for local development in the file `settings.js`. Setting it to production mode enables logging and uses the local webserver url, which is also provided in that file.

## Suggested development routine

Not everything has to be compiled. Only the plugin/popup (React) needs to be compiled by yarn after changes so it will be put into the `build` folder.

The content script, which resides in the `public` folder, does not have to be compiled and can easily be copied by using the helper script `yarn run watch-contentScript`.

__Suggestion__

Use `yarn run build-chrome` and `yarn run build-firefox` to build the chrome and firefox plugins. Then you can use `yarn run watch-contentScript-chrome` and `yarn run watch-contentScript-firefox` to watch and rebuild the injected code.

And then in chrome go to `chrome://extensions/` or in firefox go to `about:debugging#/runtime/this-firefox` and install the local plugin.

# Other notes

## Making CORS calls to the API

It cannot be made from the content script (which is injected in the page), because the API request would be made using
the `host` header as the page. The CORS of the API will block that.

> https://developer.chrome.com/docs/extensions/mv3/messaging/#simple

## Porting to Firefox

- https://extensionworkshop.com/documentation/develop/porting-a-google-chrome-extension/
- https://github.com/mozilla/webextension-polyfill
- https://www.extensiontest.com/
- https://extensionworkshop.com/documentation/develop/temporary-installation-in-firefox/
