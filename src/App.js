import { LoginContext } from "./contexts/LoginContext";
import LoginPage from "./pages/LoginPage";
import PasswordListPage from "./pages/PasswordListPage";
import { CircularProgress } from "@material-ui/core";

function App() {
	const loginContext = LoginContext.useContainer();

	if (loginContext.checkingLogin) {
		return <CircularProgress size={15} />;
	}
	return loginContext.loggedIn ? <PasswordListPage /> : <LoginPage />;
}

export default App;
