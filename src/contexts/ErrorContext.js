﻿import { useState } from "react";
import { createContainer } from "../hooks/ContextHook";
import { CommunicationModelStates } from "../src-shared/helper/communicationModel";

function useError() {
	const [errorMessage, setErrorMessage] = useState("");
	const [errorIsServerUnreachable, setErrorIsServerUnreachable] = useState(false);
	const [errorIsUnauthorized, setErrorIsUnauthorized] = useState(false);

	const clearErrors = () => {
		setErrorMessage("");
		setErrorIsServerUnreachable(false);
		setErrorIsUnauthorized(false);
	};

	const handleNetworkError = (error) => {
		setErrorMessage(error?.message);

		if (error.message === "Failed to fetch") {
			setErrorIsServerUnreachable(true);
		} else if (error?.state === CommunicationModelStates.error && error.error.httpCode === 401) {
			setErrorIsUnauthorized(true);
		} else {
			return `Es ist ein Fehler aufgetreten. Bitte versuchen Sie es später erneut. (${error})`;
		}
	};

	return {
		errorMessage,
		errorIsServerUnreachable,
		errorIsUnauthorized,
		hasErrored: errorIsServerUnreachable || errorIsUnauthorized,
		handleNetworkError,
		clearErrors,

		triggerServerIsUnreachable: () => setErrorIsServerUnreachable(true),
	};
}

export const ErrorContext = createContainer(useError);
