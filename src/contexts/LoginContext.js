﻿import { useEffect, useState } from "react";
import { createContainer } from "../hooks/ContextHook";
import { StorageManager } from "../src-shared/managers/storageManager";
import { ErrorContext } from "./ErrorContext";
import { useCommunicationManager } from "../managers/contentScriptCommunicationManager";

function useLogin() {
	const errorContext = ErrorContext.useContainer();
	const communicationManager = useCommunicationManager();

	let [myself, setMyself] = useState(/** @type {OwnUserType | null} */ null);

	// Checking if user is logged in uses async call -> we must await this before setting the loggedIn state
	let [loggedIn, setLoggedIn] = useState(false);
	let [checkingLogin, setCheckingLogin] = useState(true);

	useEffect(() => {
		const f = async () => {
			try {
				await StorageManager.getLoginPasswordHash();
				const myselfFromStorage = await StorageManager.getMyself();
				await StorageManager.getEncryptedPrivateKey();
				setLoggedIn(true);
				setMyself(myselfFromStorage);
			} catch (e) {
				await logout();
			}
		};
		f()
			.then(() => setCheckingLogin(false))
			.catch(() => setCheckingLogin(false));
	}, []);

	const logout = async (clearWebWorker = true) => {
		if (clearWebWorker) await communicationManager.sendClearWebWorkerStorage();

		setMyself(null);
		setLoggedIn(false);
		errorContext.clearErrors();
	};

	return {
		loggedIn,
		checkingLogin,
		/** @type {OwnUserType} */
		myself,
		logout,
	};
}

export const LoginContext = createContainer(useLogin);
