﻿/*global chrome*/

import { CommunicationModelStates, createRequestObject } from "../src-shared/helper/communicationModel";
import { sendRequestToWebWorker } from "../src-shared/webworkerClient";
import { LoginContext } from "../contexts/LoginContext";

export const useCommunicationManager = () => {
	const loginContext = LoginContext.useContainer();

	/**
	 *
	 * @param {CommunicationModel} request
	 * @param {string | null} tabId
	 * @returns {Promise<any>}
	 */
	const sendRequestToContentScript = async (request, tabId = null) => {
		const sendToContentScript = async (tabId) => await chrome.tabs.sendMessage(tabId, request);

		if (tabId) {
			await sendToContentScript(tabId);
		} else {
			const tabs = await chrome.tabs.query({ active: true, currentWindow: true });
			await sendToContentScript(tabs[0].id);
		}
	};

	/**
	 *
	 * @param {Promise<CommunicationModel>} responsePromise
	 * @returns {Object | string | null | SharedPasswordType[] | { httpCode: number, message: string}}
	 */
	const logoutErrorRequestWrapper = async (responsePromise) => {
		try {
			return await responsePromise;
		} catch (e) {
			if (e.httpCode === 401) await loginContext.logout(false);
			throw e;
		}
	};

	const sendFillPasswordRequestContentScript = async (login, passwordString) =>
		await logoutErrorRequestWrapper(sendRequestToContentScript(createRequestObject(CommunicationModelStates.fillPassword, { login, passwordString })));

	const sendClearWebWorkerStorage = async () => await logoutErrorRequestWrapper(sendRequestToWebWorker(CommunicationModelStates.clearWebWorkerStorage, {}));

	const sendFetchPasswordString = async (id, isAccountPassword) =>
		await logoutErrorRequestWrapper(
			sendRequestToWebWorker(CommunicationModelStates.requestPasswordString, { id: id, isAccountPassword: isAccountPassword })
		);

	const sendFetchAllPasswords = async () => await logoutErrorRequestWrapper(sendRequestToWebWorker(CommunicationModelStates.requestPasswords, null));

	const sendFetchPasswordsForUrl = async (url) => await logoutErrorRequestWrapper(sendRequestToWebWorker(CommunicationModelStates.requestPasswords, url));

	return {
		sendFillPasswordRequestContentScript,
		sendClearWebWorkerStorage,
		sendFetchPasswordString,
		sendFetchAllPasswords,
		sendFetchPasswordsForUrl,
	};
};
