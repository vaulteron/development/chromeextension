import React from "react";
import ReactDOM from "react-dom";

import App from "./App";
import { LoginContext } from "./contexts/LoginContext";
import { ErrorContext } from "./contexts/ErrorContext";

import "./index.css";
import "./styles/global.css";
import "@fortawesome/fontawesome-free/css/all.min.css";

ReactDOM.render(
	<React.StrictMode>
		<ErrorContext.Provider>
			<LoginContext.Provider>
				<App />
			</LoginContext.Provider>
		</ErrorContext.Provider>
	</React.StrictMode>,
	document.getElementById("root")
);
