﻿import { Box, Button, Grid, Tooltip, Typography } from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { ReactComponent as Logo } from "../images/Logo.svg";
import PropTypes from "prop-types";
import { ErrorContext } from "../contexts/ErrorContext";
import { Alert } from "@material-ui/lab";
import { IconTypes } from "../types/IconType";
import { LoginContext } from "../contexts/LoginContext";
import { api } from "../src-shared/managers/apiManager";
import IconButton from "./dataDisplay/IconButton";

const useStyles = makeStyles(() => ({
	paper: {
		margin: "0 auto",
		display: "flex",
		flexDirection: "column",
		width: 370,
		minHeight: "30rem",
		paddingTop: 0,
	},
	header: {
		padding: "1rem",
		borderBottom: (props) => (props.mainPage ? "1px solid black" : ""),
	},
	content: {
		padding: "1rem",
	},
	heading: {
		color: "#1D1C3E",
		marginTop: 10,
		marginBottom: 10,
	},
	logoutText: {
		textDecoration: "underline",
		marginLeft: "1rem",
		cursor: "pointer",

		"& .icon": {
			fontSize: ".7rem",
			marginRight: "3px",
			cursor: "pointer",
		},
	},
}));

const Page = ({ mainPage, children }) => {
	const classes = useStyles({ mainPage });
	const errorContext = ErrorContext.useContainer();
	const loginContext = LoginContext.useContainer();

	const email = loginContext?.myself?.email || "";

	const onLogout = () => loginContext.logout();

	const onOpenVaulteron = () => window.open(api.baseUrl, "_blank").focus();

	return (
		<Grid container className={classes.paper} direction="column">
			<Grid item className={classes.header}>
				{!mainPage && (
					<Grid item container sm={12} direction="column" alignItems="center" onClick={onOpenVaulteron} style={{ cursor: "pointer" }}>
						<Logo height="60px" />
						<Typography className={classes.heading} component="label" variant="h5" style={{ cursor: "pointer" }}>
							Vaulteron
						</Typography>
					</Grid>
				)}
				{mainPage && (
					<Grid container style={{ justifyContent: "space-between" }}>
						<Grid item>
							<Tooltip title="Vaulteron im Web öffnen" arrow>
								<Button variant="outlined" onClick={onOpenVaulteron} size="small">
									Vaulteron öffnen
								</Button>
							</Tooltip>
						</Grid>
						<Grid item style={{ display: "flex", alignItems: "center" }}>
							<Tooltip title={`${email} abmelden`} arrow>
								<IconButton iconType={IconTypes.signOut} onClick={onLogout} size="small" />
							</Tooltip>
						</Grid>
					</Grid>
				)}
			</Grid>

			<Grid item className={classes.content}>
				{errorContext.errorIsServerUnreachable && (
					<Alert severity="error">Der Vaulteron-Server ist derzeit nicht erreichbar. Bitte versuchen Sie es später noch einmal.</Alert>
				)}
				{errorContext.errorIsUnauthorized && (
					<>
						<Alert severity="error">Es gab ein Problem mit Ihrer Authentifizierung. Bitte melden Sie sich erneut an</Alert>
						<br />
						<Button variant="contained" onClick={onLogout}>
							Erneut anmelden
						</Button>
					</>
				)}

				{!errorContext.hasErrored && (
					<Grid item style={{ width: "100%" }}>
						{children}
					</Grid>
				)}
			</Grid>
		</Grid>
	);
};

Page.defaultProps = {
	mainPage: false,
};

Page.propTypes = {
	mainPage: PropTypes.bool,
	children: PropTypes.node.isRequired,
};

export default Page;
