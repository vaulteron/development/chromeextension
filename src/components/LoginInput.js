import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";

const LoginInput = (props) => {
	const { onChange } = props;

	return <TextField variant="outlined" margin="normal" required fullWidth {...props} onChange={(e) => onChange(e.target.value)} />;
};

LoginInput.defaultProps = {
	type: "text",
};

LoginInput.propTypes = {
	value: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	id: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	autoComplete: PropTypes.string.isRequired,
	type: PropTypes.oneOf(["text", "password"]),
	autoFocus: PropTypes.bool,
};

export default LoginInput;
