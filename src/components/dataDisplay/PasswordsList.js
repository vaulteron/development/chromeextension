﻿import { Avatar, Box, Grid, IconButton as MuiIconButton, List, ListItem, ListItemAvatar, ListItemText, Tooltip } from "@material-ui/core";
import { IconTypes } from "../../types/IconType";
import Icon from "./Icon";
import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { ErrorContext } from "../../contexts/ErrorContext";
import PropTypes from "prop-types";
import { useCommunicationManager } from "../../managers/contentScriptCommunicationManager";
import { createAndOpenNewTab, getURL } from "../../src-shared/managers/chromeManager";

const useStyles = makeStyles((theme) => ({
	root: {
		display: "flex",
		justifyContent: "center",
		flexDirection: "column",
		width: "100%",
		backgroundColor: theme.palette.background.paper,
		padding: 0,
		borderTop: "1px solid #e1e1e1",
	},
	listEntryContainer: {
		position: "relative",

		"&:hover > *:last-child": {
			display: "flex",
		},
	},
	typeChip: {
		marginRight: "4px",
		position: "relative",
		padding: "1.5em 1em",
		cursor: "pointer",
	},
	listItem: {
		position: "relative",
		paddingLeft: "5px",
		marginTop: "4px",
		borderBottom: "1px solid #e1e1e1",

		"& .MuiAvatar-colorDefault": {
			backgroundColor: "transparent",
		},
	},
	listContent: {
		minWidth: "15rem",
	},
	primaryText: {
		fontWeight: "bold",
		fontSize: "0.9rem",
	},
	secondaryText: {
		fontSize: " 0.7rem",
		color: "#00000073",
	},
	actionContainer: {
		display: "none",
		position: "absolute",
		top: 0,
		right: 0,
		bottom: 0,
		width: "fit-content",
		background: "white",
	},
	actions: {
		background: "#e1e1e199",
		height: "100%",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		textAlign: "left",
		borderRadius: "0",
		width: "66px",

		"&:hover": {
			background: "#e1e1e17a",
		},

		"& .icon": {
			fontSize: "2rem",
		},
	},
	actionsSubIcon: {
		right: 11,
		color: "#0000008a",
		bottom: 11,
		padding: 4,
		zIndex: "10",
		position: "absolute",
		fontSize: "1rem !important",
		borderRadius: "50%",
		background: "#ffffffd9",

		"&:before": {
			marginLeft: "1.5px",
		},
	},
	avatarIcon: {
		position: "absolute",
		left: 26,
		bottom: 4,
		background: "#cfcfcf",
		color: "#0000008a",
		fontSize: "1rem",
		padding: "5px",
		borderRadius: "50%",
		zIndex: 10,

		"&:before": {
			marginLeft: "1.5px",
		},
	},
	fallbackFavicon: {
		color: "#cfcfcf",
		fontSize: "2.7rem",
	},
}));

/**
 *
 * @param {SharedPasswordType[]} passwords
 * @returns {JSX.Element}
 * @constructor
 */
const PasswordList = ({ passwords }) => {
	const classes = useStyles();
	const errorContext = ErrorContext.useContainer();
	const communicationManager = useCommunicationManager();

	/**
	 *
	 * @param {SharedPasswordType} password
	 * @returns {JSX.Element}
	 */
	const generateAvatar = (password) => {
		let favicon = <Icon iconType={IconTypes.allPasswords} className={classes.fallbackFavicon} />;
		if (Boolean(password.websiteURL)) {
			try {
				const { hostname } = new URL(password.websiteURL);
				const src = `https://www.google.com/s2/favicons?domain=${hostname}&sz=128`;
				favicon = <img alt="favicon" style={{ height: 40 }} src={src} />;
			} catch (e) {
				// Bad URls will just use fallback value
			}
		}

		let type = IconTypes.sharedPassword;
		switch (password.type) {
			case "Private":
				type = IconTypes.secret;
				break;
			case "Company":
				type = IconTypes.lock;
				break;
			case "Group":
				type = IconTypes.sharedPassword;
				break;
		}
		return (
			<ListItemAvatar>
				<Avatar>{favicon}</Avatar>
				<Icon className={classes.avatarIcon} iconType={type} />
			</ListItemAvatar>
		);
	};

	/**
	 *
	 * @param {SharedPasswordType} password
	 */
	const handleClickedPassword = async (password) => {
		const currentUrl = await getURL();
		if (password.websiteURL && password.websiteURL !== "" && new URL(currentUrl).host !== new URL(password.websiteURL).host) {
			createAndOpenNewTab(password.websiteURL);
			return;
		}

		try {
			const isAccountPassword = password.type === "Private";
			const passwordString = await communicationManager.sendFetchPasswordString(password.id, isAccountPassword);
			communicationManager.sendFillPasswordRequestContentScript(password.login, passwordString).then(window.close);
		} catch (e) {
			errorContext.handleNetworkError(e);
		}
	};
	/**
	 * @param {MouseEvent} e
	 * @param {SharedPasswordType} password
	 */
	const handleCopyUsername = (e, password) => {
		e.stopPropagation();
		e.preventDefault();
		navigator.clipboard.writeText(password.login).then(window.close);
	};
	/**
	 * @param {MouseEvent} e
	 * @param {SharedPasswordType} password
	 */
	const handleCopyPassword = (e, password) => {
		e.stopPropagation();
		e.preventDefault();
		const isAccountPassword = password.type === "Private";
		communicationManager
			.sendFetchPasswordString(password.id, isAccountPassword)
			.then((response) => navigator.clipboard.writeText(response).then(window.close))
			.catch(errorContext.handleNetworkError);
	};

	if (passwords.length === 0) return <></>;

	return (
		<>
			<List className={classes.root}>
				{passwords.map((password) => (
					<Box className={classes.listEntryContainer}>
						<ListItem key={password.id} className={classes.listItem} onClick={() => handleClickedPassword(password)} disableGutters button>
							{generateAvatar(password)}
							<ListItemText
								className={classes.listContent}
								primary={
									<Grid container>
										<Grid item container direction="column">
											<Box className={classes.primaryText}>{password.name}</Box>
											<Box className={classes.secondaryText}>{password.login}</Box>
										</Grid>
									</Grid>
								}
							/>
						</ListItem>
						<Grid item container className={classes.actionContainer}>
							<Tooltip arrow title="Benutzername kopieren">
								<MuiIconButton onClick={(e) => handleCopyUsername(e, password)} className={classes.actions}>
									<Icon iconType={IconTypes.user} />
									<Icon iconType={IconTypes.copy} className={classes.actionsSubIcon} />
								</MuiIconButton>
							</Tooltip>
							<Tooltip arrow title="Passwort kopieren">
								<MuiIconButton onClick={(e) => handleCopyPassword(e, password)} className={classes.actions}>
									<Icon iconType={IconTypes.lock} />
									<Icon iconType={IconTypes.copy} className={classes.actionsSubIcon} />
								</MuiIconButton>
							</Tooltip>
						</Grid>
					</Box>
				))}
			</List>
		</>
	);
};

PasswordList.propTypes = {
	passwords: PropTypes.array.isRequired,
};

export default PasswordList;
