﻿import { arrayOf, number, oneOf, shape, string } from "prop-types";

/**
 * @typedef {string} KindOfPasswordType
 */

/**
 * KindOfPasswordTypes enum
 * @readonly
 * @enum {KindOfPasswordType}
 */
export const KindOfPasswordTypes = {
	accountPasswords: "accountPasswords",
	companyPasswords: "companyPasswords",
	groupPasswords: "groupPasswords",
	offlinePasswords: "offlinePasswords",
};

export const KindOfPasswordType = oneOf(Object.values(KindOfPasswordTypes));
