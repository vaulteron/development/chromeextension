/**
 * @typedef {Object} Vaulteron_root_query
 * @property {Array<(AccountPasswordType|null|undefined)>} [accountPasswords]
 * @property {Array<(AccountTagType|null|undefined)>} [accountTags]
 * @property {Array<(UserType|null|undefined)>} [admins]
 * @property {Array<(ChangeLogType|null|undefined)>} [changeLogs]
 * @property {Array<(SharedPasswordType|null|undefined)>} [companyPasswords]
 * @property {string} [customerBillingPortalUrl]
 * @property {string} [encryptedAccountPasswordString]
 * @property {string} [encryptedSharedPasswordString]
 * @property {Array<(GroupType|null|undefined)>} [groups]
 * @property {Array<(GroupType|null|undefined)>} [groupsByParent]
 * @property {string} [groupTree]
 * @property {Array<(MandatorType|null|undefined)>} [mandators]
 * @property {OwnUserType} [me]
 * @property {Array<(PublicKeyType|null|undefined)>} [missingEncryptionsForPassword]
 * @property {Array<(Guid|null|undefined)>} [missingEncryptionsForUser]
 * @property {Array<(SharedPasswordType|null|undefined)>} [sharedPasswords]
 * @property {Array<(SharedTagType|null|undefined)>} [sharedTags]
 * @property {Array<(UserType|null|undefined)>} [users]
 */

/**
 * @typedef {string} Guid
 */

/**
 * @typedef {Object} AccountPasswordType
 * @property {Array<(AccountPasswordAccessLogType|null|undefined)>} [accessLog]
 * @property {DateTime} [archivedAt]
 * @property {DateTime} [createdAt]
 * @property {string} [encryptedString]
 * @property {string} [id]
 * @property {string} login
 * @property {string} name
 * @property {string} notes
 * @property {OwnUserType} [owner]
 * @property {number} [securityRating]
 * @property {Array<(AccountTagType|null|undefined)>} [tags]
 * @property {DateTime} [updatedAt]
 * @property {string} websiteURL
 */

/**
 * @typedef {Object} AccountPasswordAccessLogType
 * @property {DateTime} [createdAt]
 * @property {string} [id]
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {*} DateTime
 */

/**
 * @typedef {Object} OwnUserType
 * @property {boolean} admin
 * @property {DateTime} [archivedAt]
 * @property {DateTime} [createdAt]
 * @property {string} email
 * @property {boolean} emailConfirmed
 * @property {string} firstname
 * @property {string} [foreignObjectId]
 * @property {Array<(GroupType|null|undefined)>} [groups]
 * @property {string} id
 * @property {PublicAndPrivateKeyType} keyPair
 * @property {string} lastname
 * @property {MandatorType} mandator
 * @property {DateTime} [passwordChangedAt]
 * @property {PublicKeyType} [publicKey]
 * @property {Sex} sex
 * @property {boolean} superAdmin
 * @property {Array<(TwoFATypes|null|undefined)>} [twoFATypesEnabled]
 * @property {DateTime} [updatedAt]
 * @property {Array<(UserGroupType|null|undefined)>} [usergroups]
 */

/**
 * @typedef {Object} GroupType
 * @property {DateTime} [archivedAt]
 * @property {Array<(GroupType|null|undefined)>} [children]
 * @property {number} [countPasswords]
 * @property {number} [countSubGroups]
 * @property {number} [countTags]
 * @property {number} [countUsers]
 * @property {DateTime} [createdAt]
 * @property {string} [id]
 * @property {string} name
 * @property {Array<(SharedPasswordType|null|undefined)>} [passwords]
 * @property {Array<(SharedTagType|null|undefined)>} [tags]
 * @property {DateTime} [updatedAt]
 * @property {Array<(UserType|null|undefined)>} [users]
 * @property {Array<(UserType|null|undefined)>} [usersWithAccess]
 */

/**
 * @typedef {Object} SharedPasswordType
 * @property {Array<(SharedPasswordAccessLogType|null|undefined)>} [accessLog]
 * @property {DateTime} [archivedAt]
 * @property {DateTime} [createdAt]
 * @property {UserType} [createdBy]
 * @property {string} [encryptedString]
 * @property {string} [groupId]
 * @property {string} [id]
 * @property {string} login
 * @property {string} modifyLock
 * @property {string} name
 * @property {string} notes
 * @property {number} [securityRating]
 * @property {Array<(SharedTagType|null|undefined)>} [tags]
 * @property {DateTime} [updatedAt]
 * @property {string} websiteURL
 */

/**
 * @typedef {Object} SharedPasswordAccessLogType
 * @property {DateTime} [createdAt]
 * @property {string} [id]
 * @property {DateTime} [updatedAt]
 * @property {UserType} [user]
 * @property {string} userId
 */

/**
 * @typedef {Object} UserType
 * @property {boolean} admin
 * @property {DateTime} [archivedAt]
 * @property {DateTime} [createdAt]
 * @property {string} email
 * @property {boolean} emailConfirmed
 * @property {string} firstname
 * @property {string} [foreignObjectId]
 * @property {Array<(GroupType|null|undefined)>} [groups]
 * @property {string} id
 * @property {string} lastname
 * @property {DateTime} [passwordChangedAt]
 * @property {PublicKeyType} [publicKey]
 * @property {Sex} sex
 * @property {Array<(TwoFATypes|null|undefined)>} [twoFATypesEnabled]
 * @property {DateTime} [updatedAt]
 * @property {Array<(UserGroupType|null|undefined)>} [usergroups]
 */

/**
 * @typedef {Object} PublicKeyType
 * @property {DateTime} [createdAt]
 * @property {string} [id]
 * @property {string} publicKeyString
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {("FEMALE"|"MALE"|"UNSET")} Sex
 */

/**
 * @typedef {("TOTP"|"WEB_AUTHN")} TwoFATypes
 */

/**
 * @typedef {Object} UserGroupType
 * @property {GroupType} [group]
 * @property {string} groupId
 * @property {GroupRole} groupRole
 * @property {UserType} [user]
 * @property {string} userId
 */

/**
 * @typedef {("GROUP_ADMIN"|"GROUP_EDITOR"|"GROUP_VIEWER")} GroupRole
 */

/**
 * @typedef {Object} SharedTagType
 * @property {string} [color]
 * @property {DateTime} [createdAt]
 * @property {GroupType} [group]
 * @property {string} [id]
 * @property {string} name
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {Object} PublicAndPrivateKeyType
 * @property {DateTime} [createdAt]
 * @property {string} encryptedPrivateKey
 * @property {string} [id]
 * @property {string} publicKeyString
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {Object} MandatorType
 * @property {DateTime} [createdAt]
 * @property {string} [id]
 * @property {string} name
 * @property {Long} subscriptionUserLimit
 * @property {boolean} twoFactorRequired
 * @property {boolean} isBusinessCustomer
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {Object} AccountTagType
 * @property {string} [color]
 * @property {DateTime} [createdAt]
 * @property {string} [id]
 * @property {string} name
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {Object} ChangeLogType
 * @property {DateTime} createdAt
 * @property {string} entityName
 * @property {string} id
 * @property {ChangeLogValueType} [newValue]
 * @property {ChangeLogValueType} [oldValue]
 * @property {string} propertyName
 * @property {UserType} [user]
 * @property {string} userId
 */

/**
 * @typedef {Object} ChangeLogValueType
 * @property {Guid} [entityId]
 * @property {string} name
 */

/**
 * @typedef {Object} Vaulteron_root_mutation
 * @property {AccountPasswordMutation} [accountPassword]
 * @property {AccountTagMutation} [accountTag]
 * @property {GroupMutations} [group]
 * @property {MandatorMutations} [mandator]
 * @property {SharedPasswordMutation} [sharedPassword]
 * @property {SharedTagMutation} [sharedTag]
 * @property {UserMutation} [user]
 */

/**
 * @typedef {Object} AccountPasswordMutation
 * @property {AccountPasswordType} [add]
 * @property {AccountPasswordType} [addEncryptedPassword]
 * @property {AccountPasswordType} [delete]
 * @property {AccountPasswordType} [edit]
 * @property {AccountPasswordType} [editEncryptedPassword]
 */

/**
 * @typedef {Object} AddAccountPasswordInputType
 * @property {string} encryptedPassword
 * @property {string} login
 * @property {string} name
 * @property {string} notes
 * @property {string} publicKeyId
 * @property {Array<string>} [tags]
 * @property {string} websiteUrl
 */

/**
 * @typedef {Object} AddEncryptedAccountPasswordInputType
 * @property {string} accountPasswordId
 * @property {string} encryptedPasswordString
 * @property {string} publicKeyId
 */

/**
 * @typedef {Object} EditAccountPasswordInputType
 * @property {EncryptedAccountPasswordInputType} [encryptedPassword]
 * @property {string} id
 * @property {string} login
 * @property {string} name
 * @property {string} notes
 * @property {Array<string>} [tags]
 * @property {string} websiteURL
 */

/**
 * @typedef {Object} EncryptedAccountPasswordInputType
 * @property {string} encryptedPasswordString
 * @property {string} publicKeyId
 */

/**
 * @typedef {Object} EditEncryptedAccountPasswordInputType
 * @property {string} accountPasswordId
 * @property {string} encryptedPasswordString
 * @property {string} publicKeyId
 */

/**
 * @typedef {Object} AccountTagMutation
 * @property {AccountTagType} [add]
 * @property {AccountTagType} [delete]
 * @property {AccountTagType} [edit]
 */

/**
 * @typedef {Object} AddAccountTagInputType
 * @property {string} color
 * @property {string} name
 */

/**
 * @typedef {Object} EditAccountTagInputType
 * @property {string} id
 * @property {string} name
 */

/**
 * @typedef {Object} GroupMutations
 * @property {GroupType} [add]
 * @property {UserType} [addUser]
 * @property {Array<(GroupType|null|undefined)>} [archive]
 * @property {Array<(GroupType|null|undefined)>} [delete]
 * @property {UserType} [deleteUser]
 * @property {GroupType} [edit]
 * @property {UserType} [editUser]
 * @property {GroupType} [move]
 */

/**
 * @typedef {Object} GroupInputType
 * @property {string} name
 * @property {string} [parentGroupId]
 */

/**
 * @typedef {Object} AddUserGroupType
 * @property {Array<(AddEncryptedSharedPasswordWithoutPublicKeyInputType|null|undefined)>} [encryptedSharedPasswords]
 * @property {string} groupId
 * @property {GroupRole} groupRole
 * @property {string} userId
 */

/**
 * @typedef {Object} AddEncryptedSharedPasswordWithoutPublicKeyInputType
 * @property {string} encryptedPasswordString
 * @property {string} sharedPasswordId
 */

/**
 * @typedef {Object} EditGroupInputType
 * @property {string} id
 * @property {string} name
 */

/**
 * @typedef {Object} EditUserGroupType
 * @property {string} groupId
 * @property {GroupRole} groupRole
 * @property {string} userId
 */

/**
 * @typedef {Object} EncryptedSharedPasswordInputType
 * @property {string} encryptedPasswordString
 * @property {string} publicKeyId
 * @property {string} [sharedPasswordId]
 */

/**
 * @typedef {Object} MandatorMutations
 * @property {MandatorType} [delete]
 */

/**
 * @typedef {Object} SharedPasswordMutation
 * @property {SharedPasswordType} [add]
 * @property {SharedPasswordType} [bulkSyncEncryptedPassword] - DEPRECATED: Encryptions should be provided within every mutation changing public-keys, password-text or access to passwords. Can still be used if ihe migration did not work, or if something else breaks (just in case).
 * @property {SharedPasswordType} [companyAdd]
 * @property {SharedPasswordType} [companyAddForUser]
 * @property {Array<(SharedPasswordType|null|undefined)>} [companyMoveToShared]
 * @property {SharedPasswordType} [delete]
 * @property {SharedPasswordType} [edit]
 * @property {Array<(SharedPasswordType|null|undefined)>} [moveToGroup]
 * @property {Array<(SharedPasswordType|null|undefined)>} [sharedMoveToCompany]
 */

/**
 * @typedef {Object} AddSharedPasswordInputType
 * @property {Array<(EncryptedSharedPasswordInputType|null|undefined)>} [encryptedPasswords]
 * @property {Guid} [groupId]
 * @property {string} [login]
 * @property {string} name
 * @property {string} [notes]
 * @property {Array<string>} [tags]
 * @property {string} [websiteUrl]
 */

/**
 * @typedef {Object} BulkSyncEncryptedSharedPasswordInputType
 * @property {Array<(EncryptedSharedPasswordInputType|null|undefined)>} encryptedPasswords
 * @property {string} sharedPasswordToAddTo
 */

/**
 * @typedef {Object} AddCompanyPasswordInputType
 * @property {Array<(EncryptedSharedPasswordInputType|null|undefined)>} [encryptedPasswords]
 * @property {string} [login]
 * @property {string} name
 * @property {string} [notes]
 * @property {string} [websiteUrl]
 */

/**
 * @typedef {Object} EditSharedPasswordInputType
 * @property {Array<(EncryptedSharedPasswordInputType|null|undefined)>} [encryptedPasswords]
 * @property {string} id
 * @property {string} [login]
 * @property {string} modifyLock
 * @property {string} [name]
 * @property {string} [notes]
 * @property {Array<string>} [tags]
 * @property {string} [websiteURL]
 */

/**
 * @typedef {Object} MoveSharedPasswordToGroupInputType
 * @property {string} destinationGroupId
 * @property {Array<(EncryptedSharedPasswordInputType|null|undefined)>} encryptedPasswords
 * @property {Array<(Guid|null|undefined)>} passwordIds
 */

/**
 * @typedef {Object} SharedTagMutation
 * @property {SharedTagType} [add]
 * @property {SharedTagType} [delete]
 * @property {SharedTagType} [edit]
 */

/**
 * @typedef {Object} AddSharedTagInputType
 * @property {string} color
 * @property {string} groupId
 * @property {string} name
 */

/**
 * @typedef {Object} EditSharedTagInputType
 * @property {string} groupId
 * @property {string} id
 * @property {string} name
 */

/**
 * @typedef {Object} UserMutation
 * @property {UserType} [add]
 * @property {UserType} [archive]
 * @property {UserType} [changeOwnLoginPassword]
 * @property {UserType} [edit]
 * @property {UserType} [grantAdminPermissions]
 * @property {UserType} [resendAccountActivationEmail]
 * @property {UserType} [revokeAdminPermissions]
 * @property {TwoFactorMutation} [twofactor]
 */

/**
 * @typedef {Object} AddUserInputType
 * @property {boolean} admin
 * @property {string} email
 * @property {string} encryptedPrivateKey
 * @property {Array<(AddEncryptedSharedPasswordWithoutPublicKeyInputType|null|undefined)>} [encryptedSharedPasswords]
 * @property {string} firstname
 * @property {string} [foreignObjectId]
 * @property {Array<string>} [groupsToBeAddedTo]
 * @property {string} lastname
 * @property {string} newTemporaryLoginPassword
 * @property {string} publicKey
 * @property {Sex} sex
 */

/**
 * @typedef {Object} AddEncryptedAccountPasswordWithoutPublicKeyInputType
 * @property {string} accountPasswordId
 * @property {string} encryptedPasswordString
 */

/**
 * @typedef {Object} EditUserInputType
 * @property {string} [email]
 * @property {string} [firstname]
 * @property {string} [foreignObjectId]
 * @property {string} [id]
 * @property {string} [lastname]
 * @property {Sex} [sex]
 */

/**
 * @typedef {Object} TwoFactorMutation
 * @property {UserType} [disableTotp]
 * @property {UserType} [disableWebAuthn]
 * @property {UserType} [enableTotp]
 * @property {UserType} [enableWebAuthn]
 * @property {TotpAuthenticatorKeyType} [getTotpAuthenticatorKey]
 * @property {WebauthnChallengeType} [getWebauthnChallenge]
 */

/**
 * @typedef {Object} TotpAuthenticatorKeyType
 * @property {string} authenticatorString
 * @property {string} key
 */

/**
 * @typedef {Object} WebauthnChallengeType
 * @property {Array<Byte>} challenge
 * @property {string} optionsJson
 */

/**
 * @typedef {*} Byte
 */
