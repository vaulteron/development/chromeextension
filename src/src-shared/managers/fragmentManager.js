﻿export const fragments = Object.freeze({
	query: {
		SelectCompanyPassword: `
            fragment SelectCompanyPassword on SharedPasswordType {
                id
                name
                login
                securityRating
                websiteURL
                notes
                modifyLock
                isSavedAsOfflinePassword
                tags {
                    id
                    name
                    color
                }
                updatedAt
                archivedAt
                groupId
                createdBy {
                    id
                    firstname
                    lastname
                }
                accessLog {
                    id
                    updatedAt
                    user {
                        firstname
                        lastname
                    }
                }
            }
        `,
		SelectSharedPassword: `
            fragment SelectSharedPassword on SharedPasswordType {
                id
                name
                login
                securityRating
                websiteURL
                notes
                modifyLock
                isSavedAsOfflinePassword
                tags {
                    id
                    name
                    color
                }
                updatedAt
                archivedAt
                groupId
                accessLog {
                    id
                    updatedAt
                    user {
                        firstname
                        lastname
                    }
                }
            }
        `,
		SelectPassword: `
			fragment SelectPassword on PasswordType {
				id type updatedAt createdAt name login websiteURL notes modifyLock
			}`,
		SelectAccountPassword: `
            fragment SelectAccountPassword on AccountPasswordType {
                id
                name
                login
                securityRating
                websiteURL
                notes
                tags {
                    id
                    name
                    color
                }
                updatedAt
            }
        `,
		SelectUserSimple: `
            fragment SelectUserSimple on UserType {
                id
                lastname
                firstname
                email
                sex
                admin
                emailConfirmed
                archivedAt
            }
        `,
		SelectOwnUserSimple: `
            fragment SelectOwnUserSimple on OwnUserType {
                id
                lastname
                firstname
                email
                sex
                admin
                superAdmin
                emailConfirmed
                twoFATypesEnabled
                passwordChangedAt
                keyPair {
                    id
                    encryptedPrivateKey
                    publicKeyString
                }
                usergroups {
                    groupId
                    userId
                    groupRole
                }
                mandator {
                    id
                    subscriptionUserLimit
                    subscriptionStatus
                    isBusinessCustomer
                }
            }
        `,
		SelectAccountTag: `
            fragment SelectAccountTag on AccountTagType {
                id
                name
                color
            }
        `,
		SelectSharedTag: `
            fragment SelectSharedTag on SharedTagType {
                id
                name
                color
                group {
                    id
                    name
                }
            }
        `,
		SelectGroup: `
            fragment SelectGroup on GroupType {
                id
                name
                archivedAt
                countPasswords
                countSubGroups
                countUsers
                countTags
            }
        `,
		SelectMandator: `
            fragment SelectMandator on MandatorType {
                id
                updatedAt
                createdAt
                name
                twoFactorRequired
                subscriptionUserLimit
            }
        `,
	},
});
