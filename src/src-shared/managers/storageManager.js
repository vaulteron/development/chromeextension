/*global chrome*/

import { CryptoManager } from "./cryptoManager";

/* ################################## CHROME ################################## */

const storageKeys = Object.freeze({
	me: "me",
});

/**
 *
 * @param {string} key
 * @param {string} value
 * @returns {Promise<undefined>}
 */
const saveIntoStorage = (key, value) =>
	new Promise((resolve) => {
		const obj = {};
		obj[key] = value;
		chrome.storage.local.set(obj, () => {
			resolve();
		});
	});

/**
 *
 * @param {string} key
 * @returns {Promise<string>}
 */
const getFromStorage = (key) =>
	new Promise((resolve, reject) => {
		chrome.storage.local.get([key], (result) => {
			if (result[key]) resolve(result[key]);
			else reject("No value stored in chrome storage for key: ", key);
		});
	});

/**
 *
 * @returns {Promise<OwnUserType>}
 */
const getMyself = async () => JSON.parse(await getFromStorage(storageKeys.me));
const saveMyself = async (value) => await saveIntoStorage(storageKeys.me, JSON.stringify(value));

/* ################################## SECURE STORAGE ################################## */

/**
 * @typedef SecureDataObject
 * @property {string} loginPassword
 * @property {string} encryptedPrivateKey
 */

/**
 *
 * @returns {Promise<SecureDataObject>}
 */
const loadSecureData = async () => await CryptoManager.loadSecureLocalData();

/**
 *
 * @param {SecureDataObject} data
 * @returns {Promise<void>}
 */
const saveSecureData = async (data) => await CryptoManager.saveSecureLocalData(data);

/**
 *
 * @returns {Promise<string>}
 */
const getLoginPasswordHash = async () => {
	const secureData = await loadSecureData();
	return secureData.loginPassword || null;
};
const saveLoginPasswordHash = async (value) => {
	let secureData = await loadSecureData();
	if (secureData) secureData.loginPassword = value;
	else secureData = { loginPassword: value, encryptedPrivateKey: null };
	await saveSecureData(secureData);
};

/**
 *
 * @returns {Promise<string>}
 */
const getEncryptedPrivateKey = async () => {
	const secureData = await loadSecureData();
	return secureData.encryptedPrivateKey || null;
};

const saveEncryptedPrivateKey = async (value) => {
	let secureData = await loadSecureData();
	if (secureData) secureData.encryptedPrivateKey = value;
	else secureData = { loginPassword: null, encryptedPrivateKey: value };
	await saveSecureData(secureData);
};

/* ################################## BOTH ################################## */

const clearExtensionStorage = async () => {
	for (let key of Object.values(storageKeys)) {
		chrome.storage.local.remove(key);
	}
	await CryptoManager.clearSecureLocalData();
};

export const StorageManager = {
	saveLoginPasswordHash,
	getLoginPasswordHash,
	saveEncryptedPrivateKey,
	getEncryptedPrivateKey,
	saveMyself,
	getMyself,
	clearExtensionStorage,
};
