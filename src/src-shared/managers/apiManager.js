import { apiBaseURL } from "../settings";

export const api = Object.freeze({
	baseUrl: apiBaseURL,
	login: `${apiBaseURL}/api/login`,
	twoFactorTotpLogin: `${apiBaseURL}/api/twofactortotplogin`,
	twoFactorWebAuthnChallenge: `${apiBaseURL}/api/twofactorwebauthnchallenge`,
	twoFactorWebAuthnLogin: `${apiBaseURL}/api/twofactorwebauthnlogin`,
	logout: `${apiBaseURL}/api/logout`,
	register: `${apiBaseURL}/api/register`,
	setPasswordForOwnAccountByEmailToken: `${apiBaseURL}/api/setPasswordFromEmailToken`,
	graphql: `${apiBaseURL}/api/graph`,
	requestEmailConfirmation: `${apiBaseURL}/api/requestEmailConfirmation`,
	getAllSharedPasswordsOnAccountActivation: `${apiBaseURL}/api/getPublicKeyAndSharedPasswordsFromEmailToken`,
});
