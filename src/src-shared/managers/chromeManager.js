﻿/*global chrome*/

import { apiBaseURL } from "../settings";

/**
 * Returns the URL of the current chrome tab
 * @returns {Promise<string>}
 */
export const getURL = async () => {
	const tabs = await chrome.tabs.query({ active: true, currentWindow: true });
	return tabs[0]?.url;
};

/**
 *
 * @param {number} tabId
 * @returns {Promise<string>}
 */
export const getUrlForTab = async (tabId) => {
	const tab = await chrome.tabs.get(tabId);
	return tab.url;
};

/**
 *
 * @param {string} url
 */
export const createAndOpenNewTab = (url) => {
	chrome.tabs.create({ url: url, active: true });
};

export const createAndOpenNewTabForLogin = () => {
	createAndOpenNewTab(`${apiBaseURL}/login?ext=1`);
};

export const closeVaulteronExtensionLoginPage = async () => {
	const tabs = await chrome.tabs.query({ active: true, currentWindow: true });
	if (tabs[0]?.url.includes(apiBaseURL)) chrome.tabs.remove(tabs[0].id);
};

export const getExtensionId = () => chrome.runtime.id;
export const getExtensionFileBaseUrl = () => chrome.runtime.getURL("/");
