﻿export const isProductionMode = true;

export const apiBaseURL = isProductionMode ? "https://app.vaulteron.com" : "https://localhost:3000";
