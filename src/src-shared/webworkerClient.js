﻿/*global chrome*/

import { CommunicationModelStates, createRequestObject } from "./helper/communicationModel";
import { logContent } from "./logManager";

/**
 *
 * @param {CommunicationModelStates} requestState
 * @param {Object | string | null} payload
 * @returns {Promise<any | SharedPasswordType[] | string | ErrorModel>}
 */
export const sendRequestToWebWorker = (requestState, payload) => {
	return new Promise((resolve, reject) => {
		/**
		 *
		 * @type {CommunicationModel}
		 */
		const content = createRequestObject(requestState, payload);

		logContent("sending request to webworker: ", content);

		chrome.runtime.sendMessage(content, (r) => {
			logContent("got response from webWorker: ", r);

			/**
			 * @type {CommunicationModel}
			 */
			const response = r;
			if (!response) {
				reject("Received no response from the WebWorker!", chrome.runtime.lastError);
				return;
			}

			if (response.state === CommunicationModelStates.error) {
				reject(response.error);
			} else {
				resolve(response.payload);
			}
		});
	});
};
