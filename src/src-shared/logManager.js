﻿import { isProductionMode } from "./settings";

export const log = (...data) => {
	if (!isProductionMode) console.log(...data);
};
export const warn = (...data) => {
	if (!isProductionMode) console.warn(...data);
};
export const error = (...data) => {
	if (!isProductionMode) console.error(...data);
};

export const logContent = (...data) => log("CONTENT-SCRIPT> ", ...data);
export const warnContent = (...data) => warn("CONTENT-SCRIPT> ", ...data);
export const errorContent = (...data) => error("CONTENT-SCRIPT> ", ...data);

export const logWebWorker = (...data) => log("WEBWORKER> ", ...data);
export const warnWebWorker = (...data) => warn("WEBWORKER> ", ...data);
export const errorWebWorker = (...data) => error("WEBWORKER> ", ...data);

export const logPopup = (...data) => log("POPUP> ", ...data);
export const warnPopup = (...data) => warn("POPUP> ", ...data);
export const errorPopup = (...data) => error("POPUP> ", ...data);
