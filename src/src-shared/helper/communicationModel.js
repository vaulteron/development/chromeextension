﻿/**
 * @typedef {Object} ErrorModel
 * @property {int} httpCode
 * @property {string} message
 */

/**
 * Enum for the state values
 * @readonly
 * @enum {string}
 */
export const CommunicationModelStates = Object.freeze({
	// From contentScript to background
	error: "error",
	requestPasswords: "requestPasswords",
	requestPasswordString: "requestPasswordString",
	saveUsernameAndPassword: "saveUsernameAndPassword",
	saveEnteredPassword: "saveEnteredPassword",
	getEnteredPassword: "getEnteredPassword",
	clearStoredUsernameAndPassword: "clearStoredUsernameAndPassword",

	// From popup to background
	clearWebWorkerStorage: "clearWebWorkerStorage",

	// From background/popup to contentScript
	response_emptySuccess: "response_emptySuccess",
	response_fetchedPasswords: "fetchedPasswords",
	response_fetchedPasswordString: "fetchedPasswordString",
	response_storedPassword: "storedPassword",
	fillPassword: "fillPassword",

	// From Vaulteron to background
	vaulteronLogin: "vaulteronLogin",
	getExtensionInfo: "getExtensionInfo",
});

/**
 * @typedef {Object} CommunicationModel
 * @property {CommunicationModelStates} state
 * @property {ErrorModel | null} error
 * @property {Object | string | null | SharedPasswordType[]} payload
 */

/**
 * @typedef {Object} ExtensionInfos
 * @property {boolean} isUserLoggedIn
 * @property {OwnUserType} myself
 * @property {string} extensionId
 * @property {string} baseFileUrl
 */

/**
 *
 * @param {CommunicationModelStates} requestState
 * @param {Object} payload
 * @returns {CommunicationModel}
 */
export const createRequestObject = (requestState, payload) => ({
	state: requestState,
	payload: payload,
	error: null,
});

/**
 *
 * @param {int} httpCode
 * @param {string} message
 * @returns {CommunicationModel}
 */
export const createErrorObject = (httpCode, message) => ({
	state: CommunicationModelStates.error,
	error: { httpCode, message },
	payload: null,
});

// TODO: returning a CommunicationModelStates is not necessary!
/**
 *
 * @param {CommunicationModelStates} responseState
 * @param {Object | string} responsePayload
 * @returns {CommunicationModel}
 */
export const createSuccessObject = (responseState, responsePayload) => ({
	state: responseState,
	error: null,
	payload: responsePayload,
});

export const createEmptySuccessObject = () => createSuccessObject(CommunicationModelStates.response_emptySuccess, {});
