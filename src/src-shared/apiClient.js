﻿import { StorageManager } from "./managers/storageManager";
import { CryptoManager } from "./managers/cryptoManager";
import { api } from "./managers/apiManager";
import { fragments } from "./managers/fragmentManager";
import { createErrorObject } from "./helper/communicationModel";
import { encryptPasswordForUsers } from "../managers/passwordManager";

const makeFetchRequest = async (requestObject) => {
	const response = await fetch(api.graphql, {
		headers: new Headers({ "Content-Type": "application/json" }),
		body: JSON.stringify(requestObject),
		method: "POST",
		credentials: "include",
	});

	if (response.ok) {
		try {
			return await response.json();
		} catch (e) {
			throw createErrorObject(response.status, `an error occured when trying to parse the non-200 response body to json: ${e.message}`);
		}
	} else {
		const contentType = response.headers.get("content-type");
		try {
			if (contentType && contentType.indexOf("application/json") !== -1) {
				const res = response.json();
				throw new Error(res.message);
			} else {
				const res = response.text();
				throw new Error(res.message);
			}
		} catch (e) {
			throw createErrorObject(response.status, e.message);
		}
	}
};

/**
 *
 * @param {string | null} url
 * @returns {Promise<SharedPasswordType[]>}
 */
export const fetchPasswordsForUrl = async (url = null) => {
	const requestPayload = {
		operationName: "GetAllPasswordsForUrl",
		query: `query GetAllPasswordsForUrl($url: String!) { passwordsByURL(url: $url) { ...SelectPassword } } ${fragments.query.SelectPassword}`,
		variables: { url: url || "" },
	};
	const responseJson = await makeFetchRequest(requestPayload);
	return responseJson.data.passwordsByURL;
};

/**
 *
 * @param {Guid} passwordId
 * @param {boolean} isAccountPassword
 * @returns {Promise<string>}
 */
export const fetchPasswordString = async (passwordId, isAccountPassword) => {
	let requestPayload;
	if (isAccountPassword) {
		requestPayload = {
			operationName: "encryptedAccountPasswordString",
			query: `query encryptedAccountPasswordString($passwordId: Guid!) { encryptedAccountPasswordString(passwordId: $passwordId)}`,
			variables: { passwordId: passwordId },
		};
	} else {
		requestPayload = {
			operationName: "EncryptedCompanyPasswordString",
			query: `query EncryptedCompanyPasswordString($passwordId: Guid!) { encryptedSharedPasswordString(passwordId: $passwordId) }`,
			variables: { passwordId: passwordId },
		};
	}

	const responseJson = await makeFetchRequest(requestPayload);
	const loginPassword = await StorageManager.getLoginPasswordHash();
	const encryptedPrivateKey = await StorageManager.getEncryptedPrivateKey();
	const encryptedPasswordString = isAccountPassword ? responseJson.data.encryptedAccountPasswordString : responseJson.data.encryptedSharedPasswordString;
	return await CryptoManager.decryptEncryptedPassword(encryptedPrivateKey, loginPassword, encryptedPasswordString);
};

/**
 *
 * @returns {Promise<OwnUserType>}
 */
export const fetchMyself = async () => {
	const requestPayload = {
		operationName: "GetMyself",
		query: `query GetMyself { me { ...SelectOwnUserSimple } } ${fragments.query.SelectOwnUserSimple}`,
		variables: {},
	};
	return await makeFetchRequest(requestPayload);
};

export const savePassword = async (
	myself,
	publicKeyString,
	username,
	passwordString,
	url,
	type,
	existingPasswordId = null,
	existingPasswordModifyLock = null
) => {
	switch (type) {
		case "Private":
			const passwordMutationObjectPrivate = createNewPasswordMutationObject(username, url, "", myself.keyPair.id, [], null, existingPasswordId);
			if (Boolean(existingPasswordId)) {
				delete passwordMutationObjectPrivate.keyPairId;
				passwordMutationObjectPrivate.encryptedPassword = {
					keyPairId: myself.keyPair.id,
					encryptedPasswordString: await CryptoManager.encryptWithPublicKey(passwordString, myself.keyPair.publicKeyString),
				};
				const requestPayloadPrivate = {
					operationName: "EditAccountPassword",
					query: `mutation EditAccountPassword($accountPassword: EditAccountPasswordInputType!) {
							accountPassword { edit(password: $accountPassword) { id } } }`,
					variables: { accountPassword: passwordMutationObjectPrivate },
				};
				return await makeFetchRequest(requestPayloadPrivate);
			} else {
				passwordMutationObjectPrivate.encryptedPassword = await CryptoManager.encryptWithPublicKey(passwordString, publicKeyString);
				const requestPayloadPrivate = {
					operationName: "AddAccountPassword",
					query: `mutation AddAccountPassword($accountPassword: AddAccountPasswordInputType!) {
							accountPassword { add(password: $accountPassword) { id } } }`,
					variables: { accountPassword: passwordMutationObjectPrivate },
				};
				return await makeFetchRequest(requestPayloadPrivate);
			}
		case "Company":
			const passwordMutationObjectCompany = createNewPasswordMutationObject(username, url, "", null, [], null, existingPasswordId);
			const admins = await executeLoadAdmins();
			passwordMutationObjectCompany.encryptedPasswords = await encryptPasswordForUsers(passwordString, [...admins, myself]);
			if (Boolean(existingPasswordId)) {
				passwordMutationObjectCompany.modifyLock = existingPasswordModifyLock;
				const requestPayloadCompany = {
					operationName: "EditCompanyPassword",
					query: `mutation EditCompanyPassword($password: EditSharedPasswordInputType!) {
            				sharedPassword { edit(password: $password) { id } } }`,
					variables: { password: passwordMutationObjectCompany },
				};
				return await makeFetchRequest(requestPayloadCompany);
			} else {
				const requestPayloadCompany = {
					operationName: "AddCompanyPassword",
					query: `mutation AddCompanyPassword($password: AddCompanyPasswordInputType!) {
            				sharedPassword { companyAdd(password: $password) { id } } }`,
					variables: { password: passwordMutationObjectCompany },
				};
				return await makeFetchRequest(requestPayloadCompany);
			}
		case "Group":
			throw new Error("Group passwords are not supported yet");
		default:
			throw new Error("Unknown password type");
	}
};

const createNewPasswordMutationObject = (login, website, description, keyPairId = null, tagIds = [], groupId = null, existingPasswordId = null) => {
	const url = new URL(website);
	const name = url.host;
	let newPassword = {
		name: name,
		login: login,
		notes: description || "",
	};
	if (Boolean(existingPasswordId)) newPassword.websiteURL = website || "";
	else newPassword.websiteUrl = website || "";
	if (Boolean(existingPasswordId)) newPassword.id = existingPasswordId;
	if (keyPairId) newPassword.keyPairId = keyPairId;
	if (tagIds.length > 0) newPassword.tags = tagIds;
	if (Boolean(groupId)) newPassword.groupId = groupId;
	return newPassword;
};

/**
 *
 * @returns {Promise<UserType[]>}
 */
const executeLoadAdmins = async () => {
	const requestPayload = {
		operationName: "GetAdmins",
		query: `query GetAdmins { admins { publicKey { id publicKeyString } ...SelectUserSimple } } ${fragments.query.SelectUserSimple}`,
		variables: {},
	};
	const result = await makeFetchRequest(requestPayload);
	return result.data.admins;
};
