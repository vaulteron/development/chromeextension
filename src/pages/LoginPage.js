import { Button } from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { LoginContext } from "../contexts/LoginContext";
import Page from "../components/Page";
import { createAndOpenNewTabForLogin } from "../src-shared/managers/chromeManager";

const useStyles = makeStyles((theme) => ({
	form: {
		width: "100%", // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

const LoginPage = () => {
	const classes = useStyles();
	const loginContext = LoginContext.useContainer();

	return (
		<Page mainPage={false}>
			{!loginContext.loggedIn && (
				<Button fullWidth variant="contained" color="primary" className={classes.submit} onClick={createAndOpenNewTabForLogin}>
					Einloggen
				</Button>
			)}
		</Page>
	);
};

export default LoginPage;
