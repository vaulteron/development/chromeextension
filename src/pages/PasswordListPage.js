﻿import Page from "../components/Page";
import PasswordList from "../components/dataDisplay/PasswordsList";
import TextField from "@material-ui/core/TextField";
import { useDelayedMaterialUIInput } from "../hooks/inputHook";
import React, { useEffect, useState } from "react";
import { getURL } from "../src-shared/managers/chromeManager";
import { ClientFilter } from "../managers/filterManager";
import { ErrorContext } from "../contexts/ErrorContext";
import { Box, CircularProgress, Grid, Typography } from "@material-ui/core";
import { useCommunicationManager } from "../managers/contentScriptCommunicationManager";

const PasswordListPage = () => {
	const errorContext = ErrorContext.useContainer();
	const communicationManager = useCommunicationManager();
	const { value: searchTerm, localValue: localSearchTerm, onChange: setSearchTerm } = useDelayedMaterialUIInput("");

	const [loading, setLoading] = useState(true);
	const [passwordsForUrl, setPasswordsForUrl] = useState(/** @type SharedPasswordType[] */ []);
	const [allPasswords, setAllPasswords] = useState(/** @type SharedPasswordType[] */ []);

	useEffect(() => {
		if (Boolean(searchTerm) && allPasswords.length === 0) {
			setLoading(true);
			communicationManager.sendFetchAllPasswords().then((response) => {
				setAllPasswords(response);
				setLoading(false);
			});
		}
	}, [searchTerm, allPasswords]);

	const updatePasswordList = () => {
		setLoading(true);
		getURL().then((url) => {
			communicationManager
				.sendFetchPasswordsForUrl(url)
				.then((response) => {
					setPasswordsForUrl(response);
					setLoading(false);
				})
				.catch(errorContext.handleNetworkError);
		});
	};
	useEffect(() => {
		updatePasswordList();
	}, []);

	const filteredPasswords = new ClientFilter(allPasswords).byArchived(false).bySearchTerm(["name", "login"], searchTerm).sortBy("name").build();

	const passwordsToShow = Boolean(searchTerm) ? filteredPasswords : passwordsForUrl;

	return (
		<Page mainPage>
			<TextField
				size="small"
				variant="outlined"
				placeholder="Suche Passwörter"
				value={localSearchTerm}
				onChange={setSearchTerm}
				fullWidth
				style={{ paddingBottom: "1rem" }}
			/>

			{loading && (
				<Grid container alignItems="center" justify="center">
					<CircularProgress />
				</Grid>
			)}
			{!loading && Boolean(searchTerm) && passwordsToShow.length === 0 && <Typography variant="h5">Keine passenden Passwörter gefunden</Typography>}
			{!loading && !Boolean(searchTerm) && passwordsToShow.length === 0 && (
				<Typography variant="h5">Keine Passwörter für diese Webseite vorhanden</Typography>
			)}
			{!loading && (
				<Box
					style={{
						maxHeight: "440px",
						maxWidth: "330px",
						overflowY: "auto",
						overflowX: "clip",
					}}
				>
					<PasswordList passwords={passwordsToShow} />
				</Box>
			)}
		</Page>
	);
};

export default PasswordListPage;
