﻿const fs = require("fs");

const args = process.argv;
console.log(args);

if (args.length < 3 || args[2] === "chrome") {
	console.log("### Using manifest.json for Chrome ###");

	const fileName = "./build/manifest.json";
	const manifest = require(fileName);

	delete manifest.browser_specific_settings;

	fs.writeFile(fileName, JSON.stringify(manifest), (err) => {
		if (err) return console.error(err);
	});
} else if (args[2] === "firefox") {
	console.log("### Updating manifest.json for Firefox ###");

	const fileName = "./build/manifest.json";
	const manifest = require(fileName);

	manifest.background = { scripts: ["background.js"] };
	delete manifest.externally_connectable;

	fs.writeFile(fileName, JSON.stringify(manifest), (err) => {
		if (err) return console.error(err);
	});
} else {
	console.error("### UNKNOWN VALUE SET FOR BROWSER COMPATABILITY ###");
}
